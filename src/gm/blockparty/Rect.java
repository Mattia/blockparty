package gm.blockparty;

import gm.blockparty.rendering.DrawableObject;
import processing.core.PApplet;

public class Rect extends DrawableObject {

    private float width, height;

    private int drawOrder;

    @Override
    public int getDrawOrder() {
        return drawOrder;
    }

    public Rect(float width, float height, int drawOrder) {
        this.width = width;
        this.height = height;
        this.drawOrder = drawOrder;
    }

    @Override
    public void draw() {
        PApplet p = Services.processing;
        p.rect(0, 0, width, height);
    }
}
