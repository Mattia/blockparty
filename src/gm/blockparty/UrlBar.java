package gm.blockparty;

import gm.blockparty.rendering.DrawableObject;
import processing.core.PApplet;

public class UrlBar extends DrawableObject {

    @Override
    public int getDrawOrder() {
        return Config.Render.Layer.UI_BASE;
    }

    @Override
    protected void init() {
        Label preUrlHeader = new Label();
        preUrlHeader.setText("to play go to ");
        preUrlHeader.setFontSize(45);
        preUrlHeader.transform.setParent(transform);
        preUrlHeader.transform.move(-350, 0);
        preUrlHeader.setAlpha(0.20f);
        addChild(preUrlHeader);

        Label urlLabel = new Label();
        urlLabel.setText(Config.Network.CLIENT_URL);
        urlLabel.setFontSize(45);
        urlLabel.transform.setParent(transform);
        urlLabel.transform.move(20, 0);
        urlLabel.setColor(Config.Render.PLAYER_COLORS[7]);
        addChild(urlLabel);

        Label postUrlHeader = new Label();
        postUrlHeader.setText(" on your phone");
        postUrlHeader.setFontSize(45);
        postUrlHeader.transform.setParent(transform);
        postUrlHeader.transform.move(410, 0);
        postUrlHeader.setAlpha(0.20f);
        addChild(postUrlHeader);

    }

    @Override
    public void draw() {
        PApplet p = Services.processing;
        float height = Config.Board.URL_BAR_HEIGHT * Config.Board.TILE_SIZE;
        float width = Config.Board.WIDTH * Config.Board.TILE_SIZE;
        p.fill(Config.Render.BAR_COLOR);
        p.rect(0, 0, width, height);
    }
}
