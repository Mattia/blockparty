package gm.blockparty;

import java.util.ArrayList;
import java.util.List;

import static gm.util.Util.Vec2i;

public class Shape {

    private static List<Shape> tetrominoShapes;
    private static List<Shape> bonusPieceShapes;
    private static List<List<Shape>> bonusTiers;

    // Initialize tetromino shapes
    static {
        tetrominoShapes = new ArrayList<>();

        // O shape
        tetrominoShapes.add(new Shape(new char[][][]{
                {
                        {'█', '█'},
                        {'█', '█'}
                }
        }));

        // I shape
        tetrominoShapes.add(new Shape(new char[][][]{
                {
                        {'█', '█', '█', '█'}
                },
                {
                        {' ', '█'},
                        {' ', '█'},
                        {' ', '█'},
                        {' ', '█'}
                }
        }));

        // J shape
        tetrominoShapes.add(new Shape(new char[][][]{
                {
                        {' ', '█'},
                        {' ', '█'},
                        {'█', '█'}
                },
                {
                        {'█', ' ', ' '},
                        {'█', '█', '█'}
                },
                {
                        {' ', '█', '█'},
                        {' ', '█', ' '},
                        {' ', '█', ' '}
                },
                {
                        {'█', '█', '█'},
                        {' ', ' ', '█'}
                }
        }));

        // L shape
        tetrominoShapes.add(new Shape(new char[][][]{
                {
                        {'█', ' '},
                        {'█', ' '},
                        {'█', '█'}
                },
                {
                        {'█', '█', '█'},
                        {'█', ' ', ' '}
                },
                {
                        {'█', '█'},
                        {' ', '█'},
                        {' ', '█'}
                },
                {
                        {' ', ' ', '█'},
                        {'█', '█', '█'}
                }
        }));

        // S shape
        tetrominoShapes.add(new Shape(new char[][][]{
                {
                        {' ', '█', '█'},
                        {'█', '█', ' '}
                },
                {
                        {' ', '█', ' '},
                        {' ', '█', '█'},
                        {' ', ' ', '█'}
                }
        }));

        // Z shape
        tetrominoShapes.add(new Shape(new char[][][]{
                {
                        {'█', '█', ' '},
                        {' ', '█', '█'}
                },
                {
                        {' ', '█'},
                        {'█', '█'},
                        {'█', ' '}
                }
        }));

        // T shape
        tetrominoShapes.add(new Shape(new char[][][]{
                {
                        {'█', '█', '█'},
                        {' ', '█', ' '}
                },
                {
                        {' ', '█'},
                        {'█', '█'},
                        {' ', '█'}
                },
                {
                        {' ', '█', ' '},
                        {'█', '█', '█'}
                },
                {
                        {'█', ' '},
                        {'█', '█'},
                        {'█', ' '}
                }
        }));


        // NON-STANDARD
        // Dot shape
        tetrominoShapes.add(new Shape(new char[][][]{
                {
                        {'█'}
                }
        }));

        // Dash shape
        tetrominoShapes.add(new Shape(new char[][][]{
                {
                        {'█', '█'}
                },
                {       {'█'},
                        {'█'}
                }
        }));

        // long dash shape
        tetrominoShapes.add(new Shape(new char[][][]{
                {
                        {'█', '█', '█'}
                },
                {       {'█'},
                        {'█'},
                        {'█'}
                }
        }));

        // right corner shape
        tetrominoShapes.add(new Shape(new char[][][]{
                {
                        {'█', '█'},
                        {'█', ' '}
                },
                {
                        {'█', '█'},
                        {' ', '█'}
                },
                {
                        {' ', '█'},
                        {'█', '█'}
                },
                {
                        {'█', ' '},
                        {'█', '█'}
                }
        }));

        // left corner shape
        tetrominoShapes.add(new Shape(new char[][][]{
                {
                        {'█', '█'},
                        {' ', '█'}
                },
                {
                        {' ', '█'},
                        {'█', '█'}
                },
                {
                        {'█', ' '},
                        {'█', '█'}
                },
                {
                        {'█', '█'},
                        {'█', ' '}
                }
        }));


    }

    // Initialize bonus shapes
    static {
        bonusPieceShapes = new ArrayList<>();

        // LEVEL 1

        // O shape
        bonusPieceShapes.add(new Shape(new char[][][]{
                {
                        {'█', '█'},
                        {'█', '█'}
                }
        }));

        bonusPieceShapes.add(new Shape(new char[][][]{
                {
                        {'█', ' ', ' '},
                        {'█', '█', '█'}
                }
        }));

        bonusPieceShapes.add(new Shape(new char[][][]{
                {
                        {' ', ' ', '█'},
                        {'█', '█', '█'}
                }
        }));

        bonusPieceShapes.add(new Shape(new char[][][]{
                {
                        {' ', '█', ' '},
                        {'█', '█', '█'}
                }
        }));

        bonusPieceShapes.add(new Shape(new char[][][]{
                {
                        {'█', '█', ' '},
                        {' ', '█', '█'}
                }
        }));

        bonusPieceShapes.add(new Shape(new char[][][]{
                {
                        {' ', '█', '█'},
                        {'█', '█', ' '}
                }
        }));


        // LEVEL 2
        // Double O shape
        bonusPieceShapes.add(new Shape(new char[][][]{
                {
                        {'█', '█'},
                        {'█', '█'},
                        {'█', '█'},
                        {'█', '█'}
                }
        }));

        // OZ shape
        bonusPieceShapes.add(new Shape(new char[][][]{
                {       {'█', '█', ' '},
                        {' ', '█', '█'},
                        {' ', '█', '█'},
                        {' ', '█', '█'}
                }
        }));

        // IL shape
        bonusPieceShapes.add(new Shape(new char[][][]{
                {       {'█', ' ', ' '},
                        {'█', '█', ' '},
                        {'█', '█', ' '},
                        {'█', '█', '█'}
                }
        }));

        // IJ shape
        bonusPieceShapes.add(new Shape(new char[][][]{
                {       {'█', ' ', ' '},
                        {'█', ' ', '█'},
                        {'█', ' ', '█'},
                        {'█', '█', '█'}
                }
        }));

        // TT
        bonusPieceShapes.add(new Shape(new char[][][]{
                {       {'█', ' ', ' '},
                        {'█', '█', '█'},
                        {'█', '█', '█'},
                        {' ', ' ', '█'}
                }
        }));

        // TT
        bonusPieceShapes.add(new Shape(new char[][][]{
                {       {'█', ' ', ' '},
                        {'█', '█', '█'},
                        {'█', '█', '█'},
                        {' ', ' ', '█'}
                }
        }));

        // LT
        bonusPieceShapes.add(new Shape(new char[][][]{
                {       {'█', '█', '█'},
                        {'█', '█', '█'},
                        {'█', ' ', '█'}
                }
        }));

        // LEVEL 3
        bonusPieceShapes.add(new Shape(new char[][][]{
                {
                        {'█', '█', '█'},
                        {'█', '█', '█'},
                        {'█', '█', '█'},
                        {'█', '█', '█'}
                }
        }));

        bonusPieceShapes.add(new Shape(new char[][][]{
                {
                        {' ', ' ', ' ', '█', ' '},
                        {'█', ' ', ' ', '█', '█'},
                        {'█', '█', '█', '█', ' '},
                        {' ', '█', '█', '█', ' '},
                        {' ', ' ', '█', ' ', ' '}
                }
        }));

        bonusPieceShapes.add(new Shape(new char[][][]{
                {
                        {' ', ' ', ' ', '█', '█'},
                        {'█', ' ', ' ', '█', '█'},
                        {'█', '█', '█', '█', ' '},
                        {' ', '█', '█', '█', ' '},
                        {' ', '█', ' ', '█', ' '},
                        {' ', '█', ' ', '█', ' '}
                }
        }));

        bonusPieceShapes.add(new Shape(new char[][][]{
                {
                        {'█', '█', ' ', ' '},
                        {'█', '█', ' ', ' '},
                        {'█', '█', '█', '█'},
                        {'█', '█', '█', '█'}
                }
        }));

        bonusPieceShapes.add(new Shape(new char[][][]{
                {
                        {' ', '█', '█', ' '},
                        {'█', '█', '█', ' '},
                        {'█', '█', '█', ' '},
                        {'█', '█', '█', '█'}
                }
        }));

        bonusPieceShapes.add(new Shape(new char[][][]{
                {
                        {' ', '█', '█', ' '},
                        {'█', '█', '█', '█'},
                        {'█', '█', '█', '█'},
                        {' ', '█', '█', ' '}
                }
        }));

        bonusPieceShapes.add(new Shape(new char[][][]{
                {
                        {'█', '█', '█', '█'},
                        {' ', '█', '█', ' '},
                        {'█', '█', '█', '█'},
                        {'█', ' ', ' ', '█'}
                }
        }));

        // LEVEL 4
        bonusPieceShapes.add(new Shape(new char[][][]{
                {
                        {'█', '█', '█', '█', ' '},
                        {' ', '█', '█', '█', '█'},
                        {' ', '█', '█', ' ', ' '},
                        {' ', '█', '█', '█', ' '},
                        {' ', '█', '█', '█', ' '}
                }
        }));

        bonusPieceShapes.add(new Shape(new char[][][]{
                {
                        {'█', '█', '█', '█', ' '},
                        {' ', '█', '█', '█', '█'},
                        {' ', '█', '█', ' ', ' '},
                        {' ', '█', '█', '█', ' '},
                        {' ', '█', '█', '█', ' '}
                }
        }));

        bonusPieceShapes.add(new Shape(new char[][][]{
                {
                        {' ', '█', '█', ' '},
                        {'█', '█', '█', '█'},
                        {'█', '█', '█', '█'},
                        {' ', '█', '█', ' '},
                        {'█', '█', '█', '█'}
                }
        }));

        bonusTiers = new ArrayList<>();
        List<Shape> shapes = getBonusShapes();
        shapes.sort((s1, s2)->s1.getNumTiles() - s2.getNumTiles());
        int lastNumTiles = -1;
        ArrayList<Shape> currentTier = null;
        for (Shape s : shapes) {
            if (lastNumTiles != s.getNumTiles()) {
                lastNumTiles = s.getNumTiles();
                currentTier = new ArrayList<>();
                bonusTiers.add(currentTier);
            }
            assert currentTier != null;
            currentTier.add(s);
        }

    }

    final private Vec2i[][] rotations;

    private Shape(char[][][] input) {

        rotations = new Vec2i[input.length][];
        for (int rotation = 0; rotation < input.length; rotation++) {
            ArrayList<Vec2i> temp = new ArrayList<>();
            char[][] chars = input[rotation];

            final int numCols = chars.length;
            final int numRows = chars[0].length;
            for (int i = 0; i < numCols; i++) {
                for (int j = 0; j < numRows; j++) {
                    if (chars[i][j] == '█') {
                        Vec2i offset = new Vec2i(j, i);
                        temp.add(offset);
                    }
                }
            }
            rotations[rotation] = temp.toArray(new Vec2i[temp.size()]);
        }
    }

    public int getNumTiles() {
        return rotations[0].length;
    }

    public static List<Shape> getBonusShapes() {
        return new ArrayList<>(bonusPieceShapes);
    }

    public static int getNumTiers() {
        return bonusTiers.size();
    }

    public static Shape getRandomShapeWithTier(int tier) {
        assert tier >= 0;
        assert bonusTiers.size() >= tier;
        List<Shape> t = bonusTiers.get(tier);
        final int si = Math.abs(Services.rng.nextInt()) % t.size();
        return t.get(si);
    }

    public static List<Shape> getTetrominoShapes() {
        return new ArrayList<>(tetrominoShapes);
    }

    public Vec2i[] getRotated(int index) {
        return rotations[index % rotations.length];
    }

}