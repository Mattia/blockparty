package gm.blockparty;

import gm.blockparty.rendering.Renderer;
import gm.scenes.SceneManager;
import gm.sound.SoundManager;
import gm.testclients.TestClientManager;
import gm.util.EventManager;
import gm.util.FontManager;
import gm.util.Time;
import processing.core.PApplet;
import processing.event.KeyEvent;

import java.util.Random;

public class BlockParty extends PApplet {
	public static void main(String args[]) {
		PApplet.main(new String[]{"gm.blockparty.BlockParty"});
	}

	@Override
	public void settings() {
		if (Config.Window.IS_FULLSCREEN) {
            fullScreen(2);
        } else {
			size((int)(Config.Window.WIDTH * Config.Window.SCALE), (int)(Config.Window.HEIGHT * Config.Window.SCALE), P2D);
		}
	}

	@Override
	public void setup() {
		smooth();

		Services.processing = this;
		Services.rng = new Random();
		Services.time = new Time(this);
		Services.events = new EventManager();
		Services.renderer = new Renderer();
		Services.fonts = new FontManager();
		Services.scenes = new SceneManager();
		Services.router = new ConnectionManager();
		Services.text = new Text();
		Services.sounds = new SoundManager();
		Services.highScores = new HighScoreManager();
		Services.scenes.pushScene(MainScene.class);
        Services.sounds = new SoundManager();

		if (Config.Testing.IS_TESTING) {
           TestClientManager.start();
        }
	}

	@Override
	public void draw() {
		// Update
		Services.events.processQueuedEvents();
		Services.scenes.update();

		// Draw
		Services.renderer.draw();
	}

	@Override
	public void keyPressed(KeyEvent event) {
		char key = event.getKey();
		gm.events.KeyEvent e = new gm.events.KeyEvent(key, true);
		Services.events.queueEvent(e);
	}

	@Override
	public void keyReleased(KeyEvent event) {
		char key = event.getKey();
		gm.events.KeyEvent e = new gm.events.KeyEvent(key, false);
		Services.events.queueEvent(e);
	}

	@Override
	public void stop() {
		Services.game.destroy();
	}

}
