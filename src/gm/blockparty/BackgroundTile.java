package gm.blockparty;

import gm.blockparty.tasks.LerpColorTask;
import gm.tasks.Task;
import gm.util.Easing;
import gm.util.Util;
import processing.core.PApplet;

class BackgroundTile extends Tile {

    private static final int NUM_SHADES = 3;
    private static final float MIN_SHADE = 0.5f;
    private static final float MAX_SHADE = 1.0f;
    private static final int PALETTE_SIZE = 8;
    private static float[] palette;

    public static final int DEFAULT_TINT = Config.Board.CLOSED_TILE_TINT;
    private float shades[];

    static {
        palette = new float[PALETTE_SIZE];

        float inc = 1.0f/PALETTE_SIZE;
        for (int i = 0; i < palette.length; i++) {
            float f = inc * (i+1);
            palette[i] = MIN_SHADE + (MAX_SHADE - MIN_SHADE) * f;
        }
    }

    public BackgroundTile() {
        this.color = DEFAULT_TINT;
        this.shades = new float[NUM_SHADES];
        for (int i = 0; i < shades.length; i++) {
            shades[i] = getRandomShade();
        }
    }

    private static float getRandomShade() {
        int i = Services.rng.nextInt(palette.length);
        return palette[i];
    }

    public void animateTint(int targetTint, float duration) {
        animateTint(targetTint, duration, false);
    }

    private int getBaseColor() {
        int x = getCell().getPosition().x;
        if (x >= Services.game.getBoard().getLeftBorder() && x < Services.game.getBoard().getRightBorder()) {
            return Config.Board.OPEN_TILE_TINT;
        } else {
            return Config.Board.CLOSED_TILE_TINT;
        }
    }

    public void animateTint(int targetTint, float duration, boolean changeIsTemporary) {
        int oldColor = getBaseColor();// this.color;
        Task animateTintTask = new LerpColorTask(this, oldColor, targetTint, duration, Easing.Quad::easeIn);
        if (changeIsTemporary) {
            animateTintTask.then(new LerpColorTask(this, targetTint, oldColor, duration, Easing.Quad::easeIn));
        }
        addTask(animateTintTask);
    }

    @Override
    public int getDrawOrder() {
        return Config.Render.Layer.BACKGROUND;
    }

    @Override
    public void draw() {
        PApplet p = Services.processing;
        p.noStroke();

        float scale = 1;
        for (int i = 0; i < NUM_SHADES; i++) {
            int c = Util.lerpColor(0xFF000000, getColor(), shades[i]);
            p.fill(c);
            p.rect(0, 0, Config.Board.TILE_SIZE * scale, Config.Board.TILE_SIZE * scale);
            scale *= 0.5;
        }

    }

}
