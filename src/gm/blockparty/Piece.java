package gm.blockparty;

import gm.util.GameObject;

import java.util.ArrayList;
import java.util.List;

import static gm.util.Util.Vec2i;

public abstract class Piece <T extends Tile> extends GameObject {

    static int nextPieceId = 1;
    private final int id;
    protected Shape shape;
    protected List<T> tiles;
    private Grid.Transform gridTransform;

    public Piece(Shape shape, Grid.Transform transform, Class<T> clazz) {
        this.id = nextPieceId++;
        this.shape = shape;
        this.gridTransform = new Grid.Transform(transform);

        this.tiles = new ArrayList<>();
        for (int i = 0; i < shape.getRotated(0).length; i++) {
            try {
                T tile = clazz.newInstance();
                tile.setPieceId(this.getId());
                tiles.add(tile);
            } catch (IllegalAccessException
                    | InstantiationException e) {
                e.printStackTrace();
            }
        }
    }

    public List<T> tilesThatIntersectWith(Piece piece) {
        Vec2i[] positions = getTilePositions();
        Vec2i[] otherPositions = piece.getTilePositions();

        ArrayList<T> intersectingTiles = new ArrayList<>(positions.length);
        for (int i = 0; i < positions.length; i++) {
            Vec2i p1 = positions[i];
            for (int j = 0; j < otherPositions.length; j++) {
                Vec2i p2 = otherPositions[j];
                if (p1.equals(p2)) {
                    intersectingTiles.add(tiles.get(i));
                    break;
                }
            }
        }
        return intersectingTiles;
    }

    public Vec2i[] getTilePositions() {
        return getTilePositionsWithTransform(this.gridTransform);
    }

    public Vec2i[] getTilePositionsWithTransform(Grid.Transform t) {
        Vec2i[] offsets = shape.getRotated(t.getRotation());
        Vec2i[] transformedOffsets = new Vec2i[offsets.length];
        for(int i = 0; i < offsets.length; i++) {
            transformedOffsets[i] = Vec2i.add(offsets[i], t.getPosition());
        }
        return transformedOffsets;
    }

    public List<T> getTiles() {
        return tiles;
    }

    public Grid.Transform getGridTransform() {
        return gridTransform;
    }

    public int getId() {
        return id;
    }

    @Override
    public void onDestroy() {
        tiles.forEach(Tile::destroy);
    }
}
