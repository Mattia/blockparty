package gm.blockparty;
import gm.blockparty.tasks.BlinkTask;
import gm.util.Util;
import processing.core.PApplet;

import java.util.Set;

public class BonusTile extends Tile {

    private enum NeighborOffset {
        UP(0, -1),
        DOWN(0, 1),
        LEFT(-1, 0),
        RIGHT(1, 0);

        final public int x;
        final public int y;
        NeighborOffset(int x, int y) {
            this.x = x;
            this.y = y;
        }

        static NeighborOffset[] offsets = {UP, DOWN, LEFT, RIGHT};
    }

    private boolean isCovered;

    @Override
    public int getDrawOrder() {
        return Config.Render.Layer.SCORING_TILE;
    }

    @Override
    public void draw() {
        PApplet p = Services.processing;

        p.strokeWeight(3);
        p.stroke(getColor());

        // draw the outline
        // check every neighbor, if there isn't a tile with the same piece id draw a line
        Grid grid = getCell().getGrid();
        for (NeighborOffset offset : NeighborOffset.offsets) {
            Util.Vec2i pos = new Util.Vec2i(getCell().getPosition());
            pos.x += offset.x;
            pos.y += offset.y;
            if (grid.isPositionInBounds(pos)) {
                Grid.Cell neighborCell = grid.getCellAt(pos);
                Set<Tile> tiles = neighborCell.getTiles();
                boolean shouldDraw = true;
                for (Tile t : tiles) {
                    if (t.getPieceId() == this.getPieceId()) {
                        shouldDraw = false;
                        break;
                    }
                }
                final float hts = Config.Board.TILE_SIZE / 2;
                if (shouldDraw) {
                    switch (offset) {
                        case UP:
                            p.line(-hts, -hts, hts, -hts);
                            break;
                        case DOWN:
                            p.line(-hts, hts, hts, hts);
                            break;
                        case LEFT:
                            p.line(-hts, -hts, -hts, hts);
                            break;
                        case RIGHT:
                            p.line(hts, -hts, hts, hts);
                            break;
                    }
                }
            }
        }

        // draw the interior
        p.strokeWeight(1);
        int c = Util.setAlpha(getColor(), 0.5f * getAlpha());
        p.noStroke();
        p.fill(c);
        float s = Config.Board.TILE_SIZE * 0.25f;
        p.rect(0, 0, s, s);
    }

    @Override
    protected void init() {
        addTask(new BlinkTask(this, 2, 6));
        super.init();
    }

    @Override
    public boolean canMoveOnto(Tile otherTile) {
        boolean isBlocked = otherTile.isLocked() || otherTile.getClass().equals(BonusTile.class);
        return !isBlocked;
    }

    boolean isCovered() {
        return isCovered;
    }

    void setCovered(boolean newState) {
        isCovered = newState;
    }

}