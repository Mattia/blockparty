package gm.blockparty;

import java.util.HashMap;
import java.util.Map;

import static gm.util.Util.Vec2i;

public enum Move {
    LEFT,
    RIGHT,
    DOWN,
    ROTATE;

    private static Map<String, Move> movesByString;

    static {
        LEFT.offset = new Vec2i(-1, 0);
        RIGHT.offset = new Vec2i(1, 0);
        DOWN.offset = new Vec2i(0, 1);
        ROTATE.offset = new Vec2i();

        LEFT.string = "LEFT";
        RIGHT.string = "RIGHT";
        DOWN.string = "DOWN";
        ROTATE.string = "ROTATE";

        movesByString = new HashMap<>();
        movesByString.put(LEFT.string, LEFT);
        movesByString.put(RIGHT.string, RIGHT);
        movesByString.put(DOWN.string, DOWN);
        movesByString.put(ROTATE.string, ROTATE);

    }

    private Vec2i offset;
    private String string;

    public static Move getMoveWithString(String string) {
        if (movesByString.containsKey(string)) {
            return movesByString.get(string);
        } else {
            throw new IllegalArgumentException(string + " does not map to a move");
        }
    }

    public Vec2i getOffset() {
        return offset;
    }

    public String getString() {
        return string;
    }

}
