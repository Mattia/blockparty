package gm.blockparty;

public class Messages {
    public static final String CONNECTED_TO_GAME = "CONNECTED_TO_GAME";
    public static final String JOIN_REQUEST = "JOIN_REQUEST";
    public static final String JOIN_RESPONSE = "JOIN_RESPONSE";
    public static final String COUNTDOWN_TICK = "COUNTDOWN_TICK";
    public static final String GAME_STARTING = "GAME_STARTING";
    public static final String GAME_EMPTY = "GAME_EMPTY";
    public static final String GAME_OVER = "GAME_OVER";
    public static final String GAME_RESET = "GAME_RESET";
    public static final String SCORE_UPDATE = "SCORE_UPDATE";
    public static final String PLAYER_MOVE_STARTED = "PLAYER_MOVE_STARTED";
    public static final String PLAYER_MOVE_ENDED = "PLAYER_MOVE_ENDED";
}
