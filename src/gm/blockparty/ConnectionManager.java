package gm.blockparty;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import gm.events.*;
import gm.glue.Client;
import gm.glue.Event;
import gm.glue.JavaWebSocket;
import gm.util.Util;

import java.util.HashSet;
import java.util.Set;

public class ConnectionManager {


    private Client client;
    private Set<Long> connectedClients = new HashSet<>();

    public ConnectionManager() {
        initClient();
        registerForEvents();
    }

    private void initClient() {
        client = new Client(new JavaWebSocket(Config.Network.ROUTER_URL));

        client.onWelcome = welcomeData -> Util.log("CONNECTED TO SERVER. ID: " + client.getId());

        client.on(Event.CLIENT_CONNECTED, data -> {
            JsonObject jsonObject = data.getAsJsonObject();
            long id = jsonObject.get("ID").getAsLong();
            JsonObject connectionData = new JsonObject();
            connectionData.add("game_id", new JsonPrimitive(client.getId()));

            connectionData.add("can_join", new JsonPrimitive(Services.game.playersCanJoin()));
            // TODO: Replace this with a queued event
            client.createMessage(Messages.CONNECTED_TO_GAME).toClients(id).withData(connectionData).send();

            if (!Config.Testing.IS_TESTING) assert !connectedClients.contains(id);
            connectedClients.add(id);
        });

        client.on(Event.CLIENT_DISCONNECTED, data -> {
            JsonObject jsonObject = data.getAsJsonObject();
            long id = jsonObject.get("ID").getAsLong();
            Services.events.queueEvent(new ClientDisconnectedEvent(id));
            connectedClients.remove(id);
        });

        client.on(Messages.JOIN_REQUEST, data -> {
            JsonObject jsonObject = data.getAsJsonObject();
            long clientId = jsonObject.get("ID").getAsLong();
            if (!Config.Testing.IS_TESTING) assert connectedClients.contains(clientId);
            Services.events.queueEvent(new JoinRequestedEvent(clientId));
        });

        client.on(Messages.PLAYER_MOVE_STARTED, data -> {
            JsonObject jsonObject = data.getAsJsonObject();
            long id = jsonObject.get("ID").getAsLong();
            Move move = Move.getMoveWithString(jsonObject.get("move_type").getAsString());
            Services.events.queueEvent(new PlayerMoveChangedEvent(move, id, true));
        });

        client.on(Messages.PLAYER_MOVE_ENDED, data -> {
            JsonObject jsonObject = data.getAsJsonObject();
            long id = jsonObject.get("ID").getAsLong();
            Move move = Move.getMoveWithString(jsonObject.get("move_type").getAsString());
            Services.events.queueEvent(new PlayerMoveChangedEvent(move, id, false));
        });
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // EVENT HANDLERS
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private void registerForEvents() {
        Services.events.register(this::onCountdownTick);
        Services.events.register(this::onGameStarted);
        Services.events.register(this::onGameEnded);
        Services.events.register(this::onGameReset);
        Services.events.register(this::onScoreUpdate);
        Services.events.register(this::onGameEmpty);
        Services.events.register(this::onJoinResponse);
    }

    private void onCountdownTick(CountdownTickEvent e) {
        JsonObject data = new JsonObject();
        data.add("time_remaining", new JsonPrimitive(e.timeRemaining));
        client.createMessage(Messages.COUNTDOWN_TICK).withData(data).send();
    }

    private void onGameStarted(GameStartedEvent e) {
        JsonObject data = new JsonObject();
        data.add("game_duration", new JsonPrimitive(Config.Game.GAME_DURATION));
        client.createMessage(Messages.GAME_STARTING).withData(data).send();
    }

    private void onGameEnded(GameOverEvent e) {
        JsonObject data = new JsonObject();
        data.add("final_score", new JsonPrimitive(e.finalScore));
        client.createMessage(Messages.GAME_OVER).withData(data).send();
    }

    private void onGameReset(GameResetEvent e) {
        client.createMessage(Messages.GAME_RESET).send();
    }

    private void onScoreUpdate(PointsScoredEvent e) {
        JsonObject data = new JsonObject();
        data.add("score", new JsonPrimitive(e.currentScore));
        client.createMessage(Messages.SCORE_UPDATE).withData(data).send();
    }

    private void onGameEmpty(GameEmptyEvent e) {
        client.createMessage(Messages.GAME_EMPTY).send();
    }

    private void onJoinResponse(JoinResponseEvent e) {
        JsonObject responseData = new JsonObject();
        responseData.add("request_successful", new JsonPrimitive(e.success));
        if (e.success) {
            responseData.add("countdown_duration", new JsonPrimitive((int)Services.game.getTimeRemaining()));
            responseData.add("color", new JsonPrimitive(e.color));
            responseData.add("number", new JsonPrimitive(e.displayNumber));
        }
        client.createMessage(Messages.JOIN_RESPONSE).toClients(e.id).withData(responseData).send();
    }

}