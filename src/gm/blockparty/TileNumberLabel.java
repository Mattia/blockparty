package gm.blockparty;

class TileNumberLabel extends Label {

    TileNumberLabel(int number) {
        setFontSize(15);
        setText("" + number);
    }

    @Override
    protected void postUpdate() {
        TetrominoTile tile = (TetrominoTile) getParent();
        if (tile.isLocked()) {
            tile.removeChild(this);
        }
    }
}
