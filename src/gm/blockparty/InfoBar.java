package gm.blockparty;

import gm.events.*;
import gm.blockparty.rendering.DrawableObject;
import gm.blockparty.tasks.RepeatingActionTask;
import gm.util.FSM;
import gm.util.Util;
import processing.core.PApplet;

public class InfoBar extends DrawableObject {

    private FSM states;

    public InfoBar() {
        states = new FSM(this);
    }

    @Override
    protected void init() {
        register(this::onGameReset);
        register(this::onGameStarted);
        register(this::onGameEnded);

        states.addState(new AttractState());
        states.addState(new PlayingState());
        states.addState(new GameOverState());
        states.transitionTo(AttractState.class);
    }

    private void onGameStarted(GameStartedEvent e) {
        states.transitionTo(PlayingState.class);
    }

    private void onGameEnded(GameOverEvent e) {
        states.transitionTo(GameOverState.class);
    }

    private void onGameReset(GameResetEvent e) {
        states.transitionTo(AttractState.class);
    }

    @Override
    public int getDrawOrder() {
        return Config.Render.Layer.UI_BASE;
    }

    @Override
    public void draw() {
        PApplet p = Services.processing;

        float height = Config.Board.INFO_BAR_HEIGHT * Config.Board.TILE_SIZE;
        float width = Config.Board.WIDTH * Config.Board.TILE_SIZE;

        p.fill(Config.Render.BAR_COLOR);
        p.rect(0, 0, width, height);
    }

    public Util.Vec2f getInfoTextAnchor() {
        return transform.getPosition();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // STATES
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private class AttractState extends FSM.State {

        Label joinLabel;

        @Override
        protected void init() {
            joinLabel = new Label();

            joinLabel.setFontSize(90);
            joinLabel.setText("BLOCK PARTY");
            joinLabel.transform.setParent(transform);
            addChild(joinLabel);
            joinLabel.setVisible(false);

            int[] colors = {
                    0xFF15D000,
                    0xFFD0E900,
                    0xFFF66F00,
                    0xFFB72121,
                    0xFFFFFFFF,
            };
            Label last = joinLabel;
            joinLabel.setColor(colors[0]);

            int i = 1;
            for (; i < colors.length; i++) {
                Label l = new Label();

                l.setFontSize(90);
                l.setText("BLOCK PARTY");

                l.setColor(colors[i]);
                l.setVisible(false);

                // Util.Vec2f p = l.transform.getPosition();

                l.addTask(new RepeatingActionTask(0.000f, ()-> {
                    float t = Services.time.elapsedTime();
                    // float y = Math.abs(Easing.Quint.easeOut(t % 1.0f));
                    float y = (float)Math.cos(t) * 0.05f;
                    // l.transform.setLocalPosition(p.x, p.y + y * 10);
                    l.transform.move(0, y);
                }));
                l.transform.setParent(last.transform);
                last.addChild(l);
                last = l;
            }

        }

        @Override
        protected void onEnter(FSM.State previousState) {
            joinLabel.setVisible(true);
        }

        @Override
        protected void onExit(FSM.State nextState) {
            joinLabel.setVisible(false);
        }

    }

    private class PlayingState extends FSM.State {

        Label scoreLabel;
        Label timeLabel;

        @Override
        protected void init() {
            register(this::onScore);
            register(this::onCountdown);

            scoreLabel = new Label();
            scoreLabel.setFontSize(30);
            scoreLabel.transform.setParent(transform);
            scoreLabel.transform.move(-500, 0);
            addChild(scoreLabel);

            timeLabel = new Label();
            timeLabel.setFontSize(30);
            timeLabel.setText("TIME: " + Config.Game.GAME_DURATION);
            timeLabel.transform.setParent(transform);
            timeLabel.transform.move(500, 0);
            addChild(timeLabel);
        }

        private void onScore(PointsScoredEvent e) {
            scoreLabel.setText("SCORE: " + e.currentScore);
        }

        private void onCountdown(CountdownTickEvent e) {
            timeLabel.setText("TIME: " + e.timeRemaining);
        }

        @Override
        protected void onEnter(FSM.State previousState) {
            scoreLabel.setText("SCORE: 0");
            scoreLabel.setVisible(true);
            timeLabel.setVisible(true);
        }

        @Override
        protected void onExit(FSM.State nextState) {
            scoreLabel.setVisible(false);
            timeLabel.setVisible(false);
        }
    }

    private class GameOverState extends FSM.State {

        Label finalScoreLabel;

        @Override
        protected void init() {
            finalScoreLabel = new Label();
            finalScoreLabel.setFontSize(30);
            finalScoreLabel.transform.setParent(transform);
            finalScoreLabel.setColor(0xFFFFFFFF);
            addChild(finalScoreLabel);
            finalScoreLabel.setVisible(false);
        }

        @Override
        protected void onEnter(FSM.State previousState) {
            finalScoreLabel.setText("FINAL SCORE: " + Services.game.getScore());
            finalScoreLabel.setVisible(true);
        }

        @Override
        protected void onExit(FSM.State nextState) {
            finalScoreLabel.setVisible(false);
        }

    }

}
