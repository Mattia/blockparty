package gm.blockparty;

import gm.events.BonusFailedEvent;
import gm.events.BonusScoredEvent;
import gm.blockparty.tasks.BlinkTask;
import gm.blockparty.tasks.DelayedActionTask;
import gm.blockparty.tasks.ImmediateActionTask;
import gm.tasks.Task;
import gm.util.Util;

public class BonusPiece extends Piece<BonusTile> {
    private int pointValue;
    private boolean isFailed;
    private Label pointsLabel;

    BonusPiece(Shape shape, Grid.Transform transform) {
        super(shape, transform, BonusTile.class);

        final int complexity = shape.getRotated(transform.getRotation()).length;
        final int complexityFactor = (int) Math.pow(complexity, Config.Score.COMPLEXITY_EXPONENT);

        int basePointValue = (Config.Score.POINTS_PER_BONUS_TILE * complexityFactor);
        pointValue = basePointValue + 50 - (basePointValue % 50);

        for (BonusTile tile : tiles) {
            tile.setColor(0xFFFFFFFF);
        }
    }

    int getPointValue() {
        return pointValue;
    }

    boolean isCompleted() {
        long numUncovered = tiles.stream().filter((BonusTile t) -> !t.isCovered()).count();
        return numUncovered == 0;
    }

    void scoreAndDestroy() {
        fireEvent(new BonusScoredEvent(this));
        for (Tile t : tiles) {
            t.getCell().clear();
        }
        this.destroy();
    }

    public Util.Vec2f getCentroid() {
        Util.Vec2f c = new Util.Vec2f();
        for (Tile t : tiles) {
            Util.Vec2f p = t.transform.getPosition();
            c.add(p);
        }
        c.mul(1.f / tiles.size());
        return c;
    }

    @Override
    public void onDestroy() {
        tiles.forEach(BonusTile::destroy);
    }

    void fail() {
        if (!isFailed) {
            fireEvent(new BonusFailedEvent(this));

            final float duration = 2.0f;

            // tint red
            Task failSequence = new ImmediateActionTask(()-> {
                for (Tile t : getTiles()) {
                    t.setColor(0xFFFF0000);
                }
            });

            // blink
            failSequence.then(new ImmediateActionTask(()->{
                for (Tile t : getTiles()) {
                    t.addTask(new BlinkTask(t, duration, 8));
                }
            }));

            // destroy
            failSequence.then(new DelayedActionTask(duration, this::destroy));
            addTask(failSequence);

            pointsLabel.setColor(0xFFFF0000);
            pointsLabel.addTask(new BlinkTask(pointsLabel, 2, 6));
        }
        isFailed = true;
    }

    void displayLabel() {
        Util.Vec2f p = getCentroid();
        // get the top most tile
        float top = Float.MAX_VALUE;
        for (Tile t : tiles) {
            if (t.transform.getPosition().y < top) {
                top = t.transform.getPosition().y;
            }
        }

        p.y = top - 25;

        if (pointsLabel != null) {
            pointsLabel.destroy();
        }
        pointsLabel = new Label();

        addChild(pointsLabel);
        pointsLabel.setText("+" + pointValue);
        pointsLabel.setColor(0xFFFFFFFF);
        pointsLabel.setFontSize(24);
        pointsLabel.transform.setLocalPosition(p);
        pointsLabel.transform.setParent(Services.game.getBoard().transform);
        pointsLabel.addTask(new BlinkTask(pointsLabel, 2, 6));
    }


}