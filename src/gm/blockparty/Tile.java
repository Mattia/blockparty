package gm.blockparty;

import gm.blockparty.rendering.DrawableObject;
import gm.util.Util;

abstract class Tile extends DrawableObject {

    private Grid.Cell cell;

    private int pieceId = 0;

    @Override
    protected void init() {
        WorldTransform boardTransform = Services.game.getBoard().transform;
        assert boardTransform != null;
        transform.setParent(boardTransform);
    }

    Grid.Cell getCell() {
        return cell;
    }

    void setCell(Grid.Cell cell) {
        this.cell = cell;
        if (this.cell != null) {
            Util.Vec2i p = cell.getPosition();
            transform.setLocalPosition(p.x * Config.Board.TILE_SIZE, p.y * Config.Board.TILE_SIZE);
        }
    }

    public boolean isSolid() {
        return false;
    }

    public boolean isLocked() {
        return true;
    }

    public boolean canMoveOnto(Tile otherTile) {
        return true;
    }

    @Override
    protected void postUpdate() {
        if (getCell() == null) {
            destroy();
        }
    }

    @Override
    public void onDestroy() {
        Grid.Cell cell = getCell();
        if (cell != null) {
            cell.removeTile(this);
        }
    }

    int getPieceId() {
        return pieceId;
    }

    void setPieceId(int pieceId) {
        this.pieceId = pieceId;
    }
}
