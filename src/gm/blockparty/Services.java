package gm.blockparty;

import gm.scenes.SceneManager;
import gm.sound.SoundManager;
import gm.blockparty.rendering.Renderer;
import gm.util.EventManager;
import gm.util.FontManager;
import gm.util.Time;
import processing.core.PApplet;

import java.util.Random;

public class Services {

	public static Time time;
	public static Game game;
	public static Random rng;
	public static Renderer renderer;
	public static FontManager fonts;
	public static PApplet processing;
	public static SceneManager scenes;
	public static EventManager events;
	public static ConnectionManager router;
	public static Text text;
	public static HighScoreManager highScores;
	public static SoundManager sounds;

}
