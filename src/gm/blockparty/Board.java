package gm.blockparty;

import gm.events.BonusScoredEvent;
import gm.events.TetrominoLockedEvent;
import gm.util.Util;

import java.util.*;
import java.util.stream.Collectors;

import static gm.util.Util.Vec2i;

public class Board extends TransformObject {

	private int bonusShapeCounter;
	private List<Shape> tetrominoShapes;
	private int width;
	private int height;
	private int center;
	private int leftBorder;
	private int rightBorder;
	private ArrayList<BonusPiece> bonusPieces;
	private Grid grid;

	Board(int w, int h) {
		width = w;
		height = h;
		center = width / 2;
		grid = new Grid(this);
		bonusPieces = new ArrayList<>();
		tetrominoShapes = new ArrayList<>();
		bonusShapeCounter = 0;
	}

	@Override
	protected void init() {
		transform.setParent(Services.scenes.getCurrentScene().getRootTransform());
		grid.populateBackgroundTiles();
		reset();
		register(this::onBonusScored); // remove the bonus piece here
		register(this::onTetrominoLocked);
	}

	Util.Vec2f getLocalPosForGridPos(int x, int y) {
		Util.Vec2f p = new Util.Vec2f();// transform.getLocalPosition();
		p.x += x * Config.Board.TILE_SIZE;
		p.y += y * Config.Board.TILE_SIZE;
		return p;
	}

	Util.Vec2f getWorldPosForGridPos(Vec2i pos) {
		return getWorldPosForGridPos(pos.x, pos.y);
	}

	Util.Vec2f getWorldPosForGridPos(int x, int y) {
		Util.Vec2f p = transform.getPosition();
		p.x += x * Config.Board.TILE_SIZE;
		p.y += y * Config.Board.TILE_SIZE;
		return p;
	}

	int getWidth() {
		return width;
	}

	int getLeftBorder() {
		return leftBorder;
	}

	int getRightBorder() {
		return rightBorder;
	}

	public BackgroundTile getBackgroundTileAt(int x, int y) {
        return grid.getCellAt(x, y).getBackgroundTile();
    }

	void reset() {
		leftBorder = center;
		rightBorder = center;

		grid.clearTiles();
		tetrominoShapes = new ArrayList<>();
		bonusPieces.forEach(BonusPiece::destroy);
		bonusPieces.clear();
		bonusShapeCounter = 0;
	}

	boolean moveTetromino(Tetromino tetromino, Move move) {
		if (grid.pieceCanMakeMove(tetromino, move)) {
			tetromino.getGridTransform().move(move);
			grid.syncTilePositions(tetromino);
			return true;
		} else {
			return false;
		}
	}

	int getHeight() {
		return height;
	}

	boolean isTetronimoStopped(Tetromino tetromino) {
		Vec2i down = new Vec2i(0, 1);
		for (Tile tile : tetromino.getTiles()) {
			Vec2i posBelow = Vec2i.add(tile.getCell().getPosition(), down);
			if (!grid.isPositionInBounds(posBelow)) {
				return true;
			}

			Grid.Cell cellBelow = grid.getCellAt(posBelow);
			Set<Tile> tiles = cellBelow.getTiles();

			for (Tile t : tiles) {
				if (t.isSolid() && t.getPieceId() != tetromino.getId() && t.isLocked()) {
					return true;
				}
			}
		}
		return false;
	}

	Tetromino createTetromino(Player player) {
		Shape shape = getNextTetrominoShape();
		Tetromino tetromino = new Tetromino(player, shape);
		player.setTetromino(tetromino);
		grid.syncTilePositions(tetromino);
		return tetromino;
	}

	private Shape getNextTetrominoShape() {
		if (tetrominoShapes.isEmpty()) {
			tetrominoShapes = Shape.getTetrominoShapes();
			Collections.shuffle(tetrominoShapes);
		}
		final int lastIndex = tetrominoShapes.size() - 1;
		Shape shape = tetrominoShapes.get(lastIndex);
		tetrominoShapes.remove(lastIndex);
		return shape;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// BONUS PIECES
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void createStartingBonusPieces(int numPieces) {
        final int slotSize = (width / numPieces);

        for (int i = 0; i < numPieces; i++) {
            int l = i * slotSize + (slotSize / 2);
            int r = l + 1;
            tryToCreateBonusPiece(l, r);
        }

	}

	boolean tryToCreateBonusPiece() {
		return tryToCreateBonusPiece(leftBorder + 1, rightBorder - 1);
	}

	boolean tryToCreateBonusPiece(int left, int right) {
		// pick a random x position
		int n = right - left;

		List<Integer> startingPositions = new ArrayList<>(n);

		for (int i = 0; i < n; i++) {
			startingPositions.add(left + i);
		}

		// shuffle the starting positions
		Collections.shuffle(startingPositions);

		// Grab a random shape
		Shape shape = Shape.getRandomShapeWithTier(bonusShapeCounter % Shape.getNumTiers());

		// Make it into a piece
		BonusPiece piece = new BonusPiece(shape, new Grid.Transform());

		// Try to place the piece into the world
		Grid.Transform t = new Grid.Transform();
		for (int x : startingPositions) {
			t.getPosition().x = x;
			for (int y = height - 1; y >= 0; --y) {
				t.getPosition().y = y;
				boolean canBePlaced = grid.pieceCanBePlacedAt(piece, t);
				if (canBePlaced) {
					// check to see if the piece is "resting" on a static piece
					Vec2i[] positions = piece.getTilePositionsWithTransform(t);
					List<Vec2i> sortedPositions = Arrays.asList(positions).stream().sorted((v1, v2) -> v2.y - v1.y).collect(Collectors.toList());
					int lastY = sortedPositions.get(0).y;
					// if the lowest row of the piece is at the bottom then all good
					boolean isOnBottom = lastY == height - 1;
					if (!isOnBottom) {
						boolean isRestingOnStatic = false;
						// otherwise check that at least one tile is resting on a static
						for (final Vec2i pos : sortedPositions) {
							Vec2i posBelow = new Vec2i(pos);
							posBelow.y++;
							Grid.Cell cellBelow = grid.getCellAt(posBelow);
							for (Tile tile : cellBelow.getTiles()) {
								if (tile.isSolid() && tile.isLocked()) {
									isRestingOnStatic = true;
									break;
								}
							}
						}
						if (!isRestingOnStatic) {
							return false;
						}
					}

					piece.getGridTransform().setPosition(t.getPosition());
					grid.syncTilePositions(piece);
					piece.displayLabel();
					bonusPieces.add(piece);
					Services.sounds.playBonusAppeared();
					bonusShapeCounter++;
					return true;
				}
			}
		}
		return false;
	}

	int getNumBonusPieces() {
		return bonusPieces.size();
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// EVENT HANDLERS
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	private void onBonusScored(BonusScoredEvent e) {
		bonusPieces.remove(e.piece);
	}

	private void onTetrominoLocked(TetrominoLockedEvent e) {

		// check to see if the tile intersects any bonus piece
		for (int i = bonusPieces.size() - 1; i >= 0; --i) {
			BonusPiece piece = bonusPieces.get(i);
			List<BonusTile> tiles = piece.tilesThatIntersectWith(e.tetromino);
			boolean tetrominoIsContained = tiles.size() == e.tetromino.getTiles().size();
			boolean tetrominoPartiallyIntersects = tiles.size() > 0;

			if (tetrominoIsContained) {
				// cover the tiles
				for (BonusTile tile : tiles) {
					tile.setCovered(true);
				}
				if (piece.isCompleted()) {
					piece.scoreAndDestroy();
                }

                // blink effect
				e.tetromino.playPlacementEffect();

            } else if (tetrominoPartiallyIntersects) {
                piece.fail();
				bonusPieces.remove(i);
			}
		}
	}

	public void setColor(int color) {
		grid.setBackgroundTileColor(color);
	}

	public void clearBackgroundTileEffects() {
		grid.clearBackgroundTileEffects();
	}
}