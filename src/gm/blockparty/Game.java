package gm.blockparty;

import gm.events.*;
import gm.blockparty.tasks.*;
import gm.tasks.Task;
import gm.util.Easing;
import gm.util.FSM;
import gm.util.GameObject;
import gm.util.Util;
import processing.core.PApplet;
import processing.core.PGraphics;

import java.util.ArrayList;
import java.util.Collection;

// TODO: Get the text to appear for bonus pieces in the center of the board

public class Game extends GameObject {

    PGraphics textBuffer;

	private FSM states;

    ArrayList<Player> availablePlayers;
    ArrayList<Player> players;

	private int score;
	private Board board;

	private PlayerBannerContainer bannerContainer;

	private boolean shouldEnd;

	Game(Board board) {
        initializePlayerPool();
        this.board = board;
		bannerContainer = new PlayerBannerContainer(board);
        textBuffer = Services.processing.createGraphics(board.getWidth(), board.getHeight());
        textBuffer.textFont(Services.fonts.getFont("Silkscreen"), 9);
	}

    private void initializePlayerPool() {
        availablePlayers = new ArrayList<>();
        players = new ArrayList<>();
        for (int i = 0; i < Config.Game.MAX_PLAYERS; i++) {
            Player player = new Player(Config.Render.PLAYER_COLORS[i], i + 1);
            availablePlayers.add(player);
        }
    }

	@Override
	protected void init() {
		register(this::onClientDisconnected);
		register(this::onBonusPieceScored);
		register(this::onBonusPieceFailed);
		register(this::onGameShouldReset);
        register(this::onJoinRequested);
		register(this::onTetrominoCannotEnter);

		this.states = new FSM(this);
		this.states.addState(new WaitingState());
		this.states.addState(new CountdownState());
		this.states.addState(new PlayingState());
		this.states.addState(new GameOverState());

		this.states.transitionTo(WaitingState.class);
	}

	public int getScore() {
		return score;
	}

	public Board getBoard() {
		return board;
	}

	boolean playersCanJoin() {
		return ((GameState)states.getCurrentState()).playersCanJoin();
	}

	private void onGameShouldReset(GameResetEvent e) {
		states.transitionTo(WaitingState.class);
	}

    private void onJoinRequested(JoinRequestedEvent e) {
	    boolean arePlayersAvailable = !availablePlayers.isEmpty();
        boolean playersCanJoin = ((GameState)states.getCurrentState()).playersCanJoin();
        boolean playerHasAlreadyJoined = players.stream().anyMatch(p->p.getId() == e.id);

        if (arePlayersAvailable && playersCanJoin && !playerHasAlreadyJoined) {
            Player player = availablePlayers.get(0); availablePlayers.remove(0);
            player.resetWithId(e.id);
            players.add(player);
            bannerContainer.addBannerForPlayer(player);
	        Services.sounds.playPlayerTone(player.getDisplayNumber()-1);
            fireEvent(new JoinResponseEvent(e.id, true, player.getColor(), player.getDisplayNumber()));
        } else {
            fireEvent(new JoinResponseEvent(e.id, false, -1, -1));
        }
    }

	synchronized Player getPlayerWithID(long id) {
		return players.stream().filter(p -> p.getId() == id).findFirst().orElse(null);
	}

	private void returnPlayerToPool(Player p) {
		assert !availablePlayers.contains(p);
		assert players.contains(p);
		if (p.getTetromino() != null) {
			p.getTetromino().destroy();
		}
        p.clearTetromino();
		players.remove(p);
		availablePlayers.add(p);
	}

	private void resetPlayerPool() {
        for (int i = players.size() - 1; i >= 0; i--) {
            Player p = players.get(i);
            returnPlayerToPool(p);
        }
		players.clear();
        assert availablePlayers.size() == Config.Game.MAX_PLAYERS;
		availablePlayers.sort((Player p1, Player p2)-> p1.getDisplayNumber() - p2.getDisplayNumber());
	}

    float getTimeRemaining() {
        return ((GameState)states.getCurrentState()).getTimeRemaining();
    }

	@Override
	public int getPriority() {
		return GameObject.HIGHEST_PRIORITY;
	}

	@Override
	protected void update() {
		states.update();
	}

	private void onTick() {
		createTetrominos();
		fireEvent(new TickEvent());
	}

	private void onStep() {
		fireEvent(new StepEvent());
	}

	private void createTetrominos() {
		for (Player player : players) {
			if (player.getTetromino() == null) {
				Tetromino tetromino = board.createTetromino(player);
				if (tetromino.isIntersectingLockedTetromino()) {
					fireEvent(new TetrominoCannotEnterEvent(tetromino));
					break;
				}
			}
		}
	}

    private void createFlyingBlockEffect(Collection<? extends Tile> tiles, float duration) {
        for (Tile tile : tiles) {
            float effectDuration = duration + ((float)Math.random());
            Rect r = new Rect(Config.Board.TILE_SIZE, Config.Board.TILE_SIZE, Config.Render.Layer.SCORING_TILE);
            Util.Vec2f pos = board.getWorldPosForGridPos(tile.getCell().getPosition());
            r.transform.setLocalPosition(pos);
            r.setColor(tile.getColor());
            r.addTask(new MoveTask(r, Util.Vec2f.add(pos, new Util.Vec2f(0, -800)), effectDuration, Easing.Quad::easeOut));
            r.addTask(new RotationTask(r, effectDuration, (float) (Math.PI * 2), Easing.Quad::easeOut));
            r.addTask(new AlphaTask(r, 1, 0, effectDuration, Easing.Quint::easeOut)).then(new ImmediateActionTask(r::destroy));
        }
    }

	private void createRisingTextEffect(String s, Util.Vec2f startPos) {
		createRisingTextEffect(s, startPos, false);
	}

	private void createRisingTextEffect(String s, Util.Vec2f startPos, boolean isBad) {
		Label effect = new InfoTextEffect(s, isBad);
		effect.transform.setLocalPosition(startPos);
	}

    private void renderBoardText(String text, int color, float duration, boolean isTemporary) {
        textBuffer.beginDraw();
        {
            textBuffer.textFont(Services.fonts.getFont("Silkscreen"), 9);
            textBuffer.noStroke();
            textBuffer.fill(255);
            textBuffer.textAlign(PApplet.CENTER, PApplet.CENTER);
            textBuffer.background(0);
            textBuffer.text(text, board.getWidth()/2 , board.getHeight()/2);
        }
        textBuffer.endDraw();

        for (int x = 0; x < board.getWidth(); x++) {
            for (int y = 0; y < board.getHeight(); y++) {
                int c = textBuffer.get(x, y);
                if (c > 0xFF000000) {
                    BackgroundTile t = board.getBackgroundTileAt(x, y);
                    t.animateTint(color, duration, isTemporary);
                }
            }
        }
    }

	private void clearPlayableArea() {
		final float minDuration = 1.5f;
		final float maxDuration = 2.5f;
		for (int x = board.getLeftBorder(); x < board.getRightBorder(); x++) {
			float duration = minDuration + (maxDuration - minDuration) * ((float)Math.random());
			float inc = duration / board.getHeight();
			for (int y = 0; y < board.getHeight(); y++) {
				BackgroundTile t = board.getBackgroundTileAt(x, y);
				t.animateTint(Config.Board.OPEN_TILE_TINT, inc * y);
			}
		}
	}

	private void fillPlayableArea() {
		final float minDuration = 1.5f;
		final float maxDuration = 2.5f;
		for (int x = board.getLeftBorder(); x < board.getRightBorder(); x++) {
			float duration = minDuration + (maxDuration - minDuration) * ((float)Math.random());
			float inc = duration / board.getHeight();
			for (int y = board.getHeight() - 1; y >= 0; y--) {
				BackgroundTile t = board.getBackgroundTileAt(x, y);
				t.animateTint(Config.Board.CLOSED_TILE_TINT, inc * (board.getHeight() - y));
			}
		}
	}

	private void flashPlayableArea(float intensity) {
		for (int x = board.getLeftBorder(); x < board.getRightBorder(); x++) {
			float duration = 0.5f;
			for (int y = 0; y < board.getHeight(); y++) {
				BackgroundTile t = board.getBackgroundTileAt(x, y);
				int c = Util.lerpColor(Config.Board.OPEN_TILE_TINT, 0xFFFF0000, intensity);
				t.animateTint(c, duration, true);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// EVENT HANDLERS
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	private void onBonusPieceScored(BonusScoredEvent e) {
		createRisingTextEffect("BONUS BLOCK SCORED\n+"+e.piece.getPointValue(), e.piece.getCentroid());
		final int scoreIncrement = e.piece.getPointValue();
		score += scoreIncrement;
		fireEvent(new PointsScoredEvent(score, scoreIncrement));
		createFlyingBlockEffect(e.piece.tiles, 5.0f);
		Services.sounds.playBonusScored();
	}

	private void onClientDisconnected(ClientDisconnectedEvent e) {
		Player player = getPlayerWithID(e.id);

		if (player != null) {
			bannerContainer.removeBannerForPlayer(player);
			returnPlayerToPool(player);
		}
	}

	private void onBonusPieceFailed(BonusFailedEvent e) {
		final String message = Services.text.bonusBrokenMessage;
		final Util.Vec2f startPos = e.piece.getCentroid();

		createRisingTextEffect(message, startPos, true);
        Services.sounds.playBonusBroken();
	}

	private void onTetrominoCannotEnter(TetrominoCannotEnterEvent e) {
		shouldEnd = true;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// STATES
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	class GameState extends FSM.State {
        public boolean playersCanJoin() {
			return false;
		}
		public float getTimeRemaining() { return 0; }
	}

	private class WaitingState extends GameState {

        Task attractTextTask;

		@Override
		public boolean playersCanJoin() {
			return true;
		}

		@Override
		protected void init() {
            float interval = 0.75f;
            attractTextTask = new WaitTask(0);

            for (String text : Services.text.attractMessages) {
                attractTextTask.then(new DelayedActionTask(interval, ()-> renderBoardText(text, 0xFF000089, interval * .5f, true)));
            }
            attractTextTask.then(new ImmediateActionTask(()->addTask(attractTextTask)));

		}

		@Override
		protected void onEnter(FSM.State previousState) {
			assert players.size() == 0;
            addTask(attractTextTask);
		}

		@Override
		protected void onExit(FSM.State nextState) {
            attractTextTask.abort();
		}

		@Override
		protected void update() {
			if (players.size() > 0) {
				transitionTo(CountdownState.class);
			}
		}
	}

	private class CountdownState extends GameState {

		@Override
		public boolean playersCanJoin() {
			return ((Game)getContext()).players.size() < Config.Game.MAX_PLAYERS;
		}

		@Override
		public float getTimeRemaining() {
			return timeRemaining;
		}

        private int secondsRemaining;
		private float timeRemaining;

		@Override
		protected void onEnter(FSM.State previousState) {
			timeRemaining = Config.Game.GAME_START_COUNTDOWN_DURATION;
            secondsRemaining = (int)timeRemaining;
            Services.sounds.playStartCountdown();
		}

        @Override
        protected void onExit(FSM.State nextState) {
            Services.sounds.stopStartCountdown();
        }

        @Override
		protected void update() {
			if (players.size() == 0) {
				transitionTo(WaitingState.class);
				Services.events.fire(new GameEmptyEvent());
			} else {
				timeRemaining -= Services.time.getDeltaTime();
				if (timeRemaining < 0 || players.size() == Config.Game.MAX_PLAYERS) {
					transitionTo(PlayingState.class);
				} else {
                    int newSecondsRemaining = (int)timeRemaining;
					if (secondsRemaining != newSecondsRemaining) {
						secondsRemaining = newSecondsRemaining;
						Services.events.fire(new CountdownTickEvent(secondsRemaining));
                        renderBoardText(""+(int)timeRemaining, Config.Board.OPEN_TILE_TINT, 0.5f, true);
					}
				}
			}
		}
	}

	private class PlayingState extends GameState {
		private float timeRemaining;
		private float timeToNextTick;
		private int elapsedTicks;
		private Task bonusPieceCreationTask;
		private Task gameCountDownTask;

		@Override
		public boolean playersCanJoin() {
			return false;
		}

		@Override
		public float getTimeRemaining() {
			return timeRemaining;
		}

		@Override
		protected void init() {
			bonusPieceCreationTask = new WaitTask(1);
			bonusPieceCreationTask.then( new RepeatingActionTask(Config.BonusPiece.BONUS_CREATION_INTERVAL, () -> {
				final int numBonusPieces = board.getNumBonusPieces();
				if (numBonusPieces < Config.BonusPiece.NUM_ON_SCREEN_BONUS_PIECES) {
					board.tryToCreateBonusPiece();
				}
			}));

            final float maxIntensity = 0.8f;
			gameCountDownTask = new RepeatingActionTask(1, () -> {
				timeRemaining -= 1;
				if (timeRemaining <= 0 || shouldEnd) {
					transitionTo(GameOverState.class);
				} else {
					fireEvent(new CountdownTickEvent((int)timeRemaining));
                    if (timeRemaining <= Config.Game.GAME_ENDING_COUNTDOWN_DURATION) {
	                    addTask(new DelayedActionTask(0.5f, Services.sounds::playCountdownTick));
                        flashPlayableArea(maxIntensity-(timeRemaining/Config.Game.GAME_ENDING_COUNTDOWN_DURATION));
                        renderBoardText(""+(int)timeRemaining, 0xFFFF7700, 0.5f, true);
                    }
				}
			});
		}

		@Override
		protected void onEnter(FSM.State previousState) {
			shouldEnd = false;
            score = 0;
			timeRemaining = Config.Game.GAME_DURATION;
			addTask(bonusPieceCreationTask);
			addTask(gameCountDownTask);
			Services.events.fire(new GameStartedEvent());

			clearPlayableArea();
			bannerContainer.clearBanners();
            Services.sounds.playGameMusic();

			board.createStartingBonusPieces(Config.BonusPiece.NUM_ON_SCREEN_BONUS_PIECES);

			board.clearBackgroundTileEffects();
			board.setColor(Config.Board.OPEN_TILE_TINT);

			final float delay = Config.Game.INFO_EFFECT_DURATION;
			Util.Vec2f pos = board.getWorldPosForGridPos(board.getWidth()/2, board.getHeight());

			Task tutorialSequence = new WaitTask(0);
			for (String s : Services.text.blockTutorialStrings) {
				tutorialSequence.then(new ImmediateActionTask(()-> createRisingTextEffect(s, pos)));
				tutorialSequence.then(new WaitTask(delay));
			}
			addTask(tutorialSequence);

		}

		@Override
		protected void onExit(FSM.State nextState) {
            bonusPieceCreationTask.abort();
			gameCountDownTask.abort();
			fireEvent(new GameOverEvent(score));
			fillPlayableArea();
			board.reset();
			resetPlayerPool();
			InfoTextEffect.clearAll();
			Services.sounds.stopGameMusic();
			Services.sounds.playGameOver();
		}

        @Override
		protected void update() {

			if (players.size() == 0) {
				transitionTo(WaitingState.class);
				Services.events.fire(new GameEmptyEvent());
			} else if (!shouldEnd) {
				float deltaTime = Services.time.getDeltaTime();
				timeToNextTick -= deltaTime;
				if (timeToNextTick <= 0) {
					elapsedTicks++;
					timeToNextTick += Config.Timing.TICK_INTERVAL;
					onTick();
				}

				if (elapsedTicks >= Config.Timing.TICKS_PER_STEP) {
					onStep();
					elapsedTicks = 0;
				}
			}
		}
    }

	private class GameOverState extends GameState {

        Task restartGameTask;
        Task gameOverTextFlash;

		@Override
		protected void init() {
            float blinkInterval = 1.0f;
            gameOverTextFlash = new RepeatingActionTask(blinkInterval, ()-> renderBoardText("GAME OVER\n \nRANKED #"+Services.highScores.getRankForScore(score), 0xFFFF0000, blinkInterval * .5f, true));
            restartGameTask = new DelayedActionTask(Config.Game.GAME_OVER_SCREEN_DURATION, ()-> Services.events.queueEvent(new GameResetEvent()));
		}

		@Override
		protected void onEnter(FSM.State previousState) {
            addTask(gameOverTextFlash);
            addTask(restartGameTask);
		}

		@Override
		protected void onExit(FSM.State nextState) {
            gameOverTextFlash.abort();
            restartGameTask.abort();
		}

	}

}
