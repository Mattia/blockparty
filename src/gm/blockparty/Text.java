package gm.blockparty;

public class Text {

    public final String[] blockTutorialStrings = {
            "These are bonus blocks",
            "Fill them exactly to score BIG",
            "Or they will break"
    };

    public final String bonusBrokenMessage = "BONUS BLOCK\nBROKEN";

    public final String[] attractMessages = {
            "WORK", "TOGETHER", "TO", "BUILD", "THE", "SHAPES.", "", "", ""
    };

}
