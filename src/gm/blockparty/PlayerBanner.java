package gm.blockparty;

import gm.blockparty.tasks.AlphaTask;
import gm.blockparty.tasks.ImmediateActionTask;
import gm.blockparty.tasks.MoveTask;
import gm.util.Easing;
import gm.util.Util;

class PlayerBanner extends TransformObject{

    private static float backgroundAlpha = 0.8f;


    private Rect background;
    private Label number;

    private final Player player;
    private MoveTask moveTask;

    PlayerBanner(Player player) {
        this.player = player;
        float w = Config.Board.PLAYER_SLOT_WIDTH * Config.Board.TILE_SIZE;
        float h = Config.Board.HEIGHT * Config.Board.TILE_SIZE;
        background = new Rect(w,h, Config.Render.Layer.UI_OVERLAY);
        background.transform.setParent(this.transform);
        background.transform.move(0, h/2);
        background.setColor(player.getColor());
        background.setAlpha(backgroundAlpha);

        number = new Label();
        number.setText("#"+player.getDisplayNumber());
        number.setFontSize(45);
        number.transform.setParent(transform);
        number.transform.move(0, 100);
        number.setColor(0xFFFFFFFF);

        addChild(background);
        addChild(number);
    }

    Player getPlayer() {
        return player;
    }

    void moveTo(Util.Vec2f pos) {

        if (moveTask != null) {
            this.transform.setLocalPosition(moveTask.getEndPos());
            moveTask.abort();
        }
        moveTask = new MoveTask(this, pos, 0.5f, Easing.Quint::easeIn);
        addTask(moveTask);
    }

    void appear() {
        background.addTask(new AlphaTask(background, 0, backgroundAlpha, 0.5f, Easing.Quint::easeIn));
        number.addTask(new AlphaTask(number, 0, 1, 0.5f, Easing.Quint::easeIn));
    }

    void disappear() {
        background.addTask(new AlphaTask(background, backgroundAlpha, 0, 0.5f, Easing.Quint::easeIn));
        number.addTask(new AlphaTask(number, 1, 0, 0.5f, Easing.Quint::easeIn)).then(new ImmediateActionTask(this::destroy));
    }

}
