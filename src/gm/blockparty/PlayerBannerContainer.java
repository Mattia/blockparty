package gm.blockparty;

import gm.util.GameObject;
import gm.util.Util;

import java.util.ArrayList;

public class PlayerBannerContainer extends TransformObject {

    private Board board;

    private final static Util.Vec2f boardOffset = new Util.Vec2f(Config.Board.TILE_SIZE/2 + Config.Board.TILE_SIZE, -Config.Board.TILE_SIZE/2); // FIX: Why is this additional 1 tile offset necessary?

    PlayerBannerContainer(Board board) {
        this.board = board;
    }

    @Override
    protected void init() {
        transform.setParent(board.transform);
    }

    void addBannerForPlayer(Player player) {
        PlayerBanner banner = new PlayerBanner(player);
        addChild(banner);
        banner.transform.setLocalPosition(board.getLocalPosForGridPos(board.getWidth()/2, 0));
        banner.transform.move(boardOffset.x, boardOffset.y);
        banner.transform.setParent(this.transform);
        banner.appear();
        assignSlots();
    }

    void removeBannerForPlayer(Player player) {
        ArrayList<GameObject> children = getChildren();
        for (int i = children.size() - 1; i >= 0; i--) {
            PlayerBanner b = (PlayerBanner)children.get(i);
            if (b.getPlayer() == player) {
                removeChild(b);
                assignSlots();
                return;
            }
        }
    }

    void clearBanners() {
        for (GameObject o : getChildren()) {
            PlayerBanner b = (PlayerBanner)o;
            b.disappear();
        }
    }

    private void assignSlots() {
        int width = Config.Board.PLAYER_SLOT_WIDTH * getChildren().size();
        int center = (board.getWidth() / 2);

        for (int i = 0; i < getChildren().size(); i++) {
            int slot = (center - width / 2) + i * Config.Board.PLAYER_SLOT_WIDTH;
            PlayerBanner banner = (PlayerBanner)getChildren().get(i);
            banner.getPlayer().setStartPosition(slot);
            Util.Vec2f bannerPos = board.getLocalPosForGridPos(slot, 0);
            bannerPos.add(boardOffset);
            banner.moveTo(bannerPos);
        }
    }

}
