package gm.blockparty;

import gm.util.GameObject;

public class TransformObject extends GameObject {
    public final WorldTransform transform = new WorldTransform();

    @Override
    public void _destroy() {
        transform.clearParent();
        for (int i = transform.getChildren().size() - 1; i >= 0; i--) {
            transform.getChildren().get(i).clearParent();
        }
        super._destroy();
    }
}
