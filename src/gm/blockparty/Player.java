package gm.blockparty;

import gm.util.GameObject;

public class Player extends GameObject {

	private Tetromino tetromino;
	private int color;
	private long id;
	private int startPosition;
	private int displayNumber;

	public Player(int color, int displayNumber) {
		this.displayNumber = displayNumber;
		this.color = color;
	}

	public void resetWithId(long id) {
		this.id = id;
	}

	public int getDisplayNumber() {
		return displayNumber;
	}

	public int getColor() {
		return color;
	}

	public Tetromino getTetromino() {
		return tetromino;
	}

	public void setTetromino(Tetromino tetromino) {
		this.tetromino = tetromino;
		this.tetromino.getGridTransform().getPosition().x = startPosition;
	}

	public int getStartPosition() {
		return startPosition;
	}

	public void setStartPosition(int startPosition) {
		this.startPosition = startPosition;
	}

	public long getId() {
		return id;
	}

	@Override
	public void onDestroy() {
		if (tetromino != null) {
			tetromino.destroy();
		}
	}

	public void clearTetromino() {
		tetromino = null;
	}
}
