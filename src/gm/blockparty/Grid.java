package gm.blockparty;

// TODO: Get rid of all the stuff having to do with moveable borders

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static gm.util.Util.Vec2i;

class Grid {

    //region Instance Variables
    private Cell[][] cells;
    private Board board;
    //endregion

    //region Constructor
    Grid(Board board) {
        this.board = board;
        cells = new Cell[this.board.getWidth()][this.board.getHeight()];

    }
    //endregion
    void clearTiles() {
        for (int x = 0; x < cells.length; ++x) {
            for (int y = 0; y < cells[x].length; ++y) {
                cells[x][y].clear();
            }
        }
    }

    boolean pieceCanBePlacedAt(Piece piece, Grid.Transform transform) {
        Vec2i[] positions = piece.getTilePositionsWithTransform(transform);

        for (int i = 0; i < positions.length; i++) {
            Vec2i position = positions[i];
            if (!isPositionInBounds(position)) {
                return false;
            }
            Tile tile = (Tile) piece.tiles.get(i);

            Cell cell = getCellAt(position);
            Set<Tile> tiles = cell.getTiles();
            for (Tile otherTile : tiles) {
                if (!tile.canMoveOnto(otherTile)) {
                    return false;
                }
            }
        }
        return true;
    }

    boolean pieceCanMakeMove(Piece piece, Move move) {
        Grid.Transform movedTransform = piece.getGridTransform().getMoved(move);
        return pieceCanBePlacedAt(piece, movedTransform);
    }

    //endregion

    // Synchronizes tile positions to a piece's shape
    <T extends Tile> void syncTilePositions(Piece<T> piece) {
        List<T> tiles = piece.getTiles();
        Vec2i[] positions = piece.getTilePositions();

        for (int i = 0; i < positions.length; i++) {
            Vec2i position = positions[i];
            T tile = tiles.get(i);
            Cell newCell = getCellAt(position);
            assert newCell != null;
            newCell.addTile(tile);
            assert tile.getCell() == newCell;
        }
    }
    //endregion

    //region Access Cells
    Cell getCellAt(Vec2i pos) {
        return getCellAt(pos.x, pos.y);
    }

    Cell getCellAt(int x, int y) {
        return cells[x][y];
    }
    //endregion

    //region Bounds Checking
    boolean isPositionInBounds(Vec2i pos) {
        return isPositionInBounds(pos.x, pos.y);
    }

    boolean isPositionInBounds(int x, int y) {
        return (x >= board.getLeftBorder()) && (x < board.getRightBorder()) && (y >= 0) && (y < this.board.getHeight());
    }

    public void populateBackgroundTiles() {
        for (int x = 0; x < this.board.getWidth(); ++x) {
            for (int y = 0; y < this.board.getHeight(); ++y) {
                Cell cell = new Cell(x, y);
                cell.setBackgroundTile(new BackgroundTile());
                cells[x][y] = cell;
            }
        }
    }

    public void setBackgroundTileColor(int color) {
        for (Cell[] column : cells) {
            for (Cell cell : column) {
                cell.getBackgroundTile().setColor(color);
            }
        }
    }

    public void clearBackgroundTileEffects() {
        for (Cell[] column : cells) {
            for (Cell cell : column) {
                cell.getBackgroundTile().clearTasks();
            }
        }
    }

    static class Transform {
        private Vec2i position;
        private int rotation;

        Transform() {
            this(new Vec2i(), 0);
        }

        Transform(Vec2i position) {
            this(position, 0);
        }

        Transform(Vec2i position, int rotation) {
            this.position = new Vec2i(position);
            this.rotation = rotation;
        }

        Transform(Grid.Transform transform) {
            this(new Vec2i(transform.position), transform.rotation);
        }

        public Vec2i getPosition() {
            return position;
        }

        void setPosition(Vec2i newPos) {
            this.position.x = newPos.x;
            this.position.y = newPos.y;
        }

        int getRotation() {
            return rotation;
        }

        void move(Move move) {
            switch (move) {
                case LEFT:
                    position.x--;
                    break;
                case RIGHT:
                    position.x++;
                    break;
                case DOWN:
                    position.y++;
                    break;
                case ROTATE:
                    rotation++;
                    break;
            }
        }

        Grid.Transform getMoved(Move move) {
            Grid.Transform t = new Grid.Transform(this);
            t.move(move);
            return t;
        }

    }
    //endregion

    //region Internal Cell Class
    class Cell {

        private Vec2i position;
        // TODO: Should these be a set? If someone double adds a tile that's an error. Not something we want to paper over
        private Set<Tile> tiles;

        private BackgroundTile backgroundTile;

        Cell(int x, int y) {
            position = new Vec2i(x, y);
            tiles = new HashSet<>();
        }

        Grid getGrid() {
            return Grid.this;
        }

        public Vec2i getPosition() {
            return position;
        }

        Set<Tile> getTiles() {
            return tiles;
        }

        void addTile(Tile tile) {
            assert tile != null;
            Cell oldCell = tile.getCell();
            if (oldCell != null) {
                oldCell.removeTile(tile);
            }
            tile.setCell(this);
            tiles.add(tile);
        }

        void removeTile(Tile tile) {
            assert tile != null;
            tile.setCell(null);
            tiles.remove(tile);
        }

        void clear() {
            for (Tile tile : tiles) {
                tile.setCell(null);
            }
            tiles.clear();
        }

        void setBackgroundTile(BackgroundTile backgroundTile) {
            this.backgroundTile = backgroundTile;
            this.backgroundTile.setCell(this);
        }

        public BackgroundTile getBackgroundTile() {
            return backgroundTile;
        }
    }
    //endregion

}
