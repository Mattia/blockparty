package gm.blockparty;

import processing.core.PApplet;

public class TetrominoTile extends Tile {

    private boolean isLocked;
    private boolean didMove;

    @Override
    public boolean canMoveOnto(Tile otherTile) {
        boolean isBlocked = otherTile.isSolid();
        boolean isSamePiece = otherTile.getPieceId() == getPieceId();
        return isSamePiece || !isBlocked;
    }

    @Override
    public boolean isSolid() {
        return didMove;
    }

    @Override
    public boolean isLocked() {
        return isLocked;
    }

    public void setLocked(boolean status) {
        isLocked = status;
    }

    @Override
    public int getDrawOrder() {
        return Config.Render.Layer.TETROMINO_TILE;
    }

    public void setDidMove(boolean didMove) {
        this.didMove = didMove;
    }

    @Override
    protected void preUpdate() {
        if (isLocked || !isSolid()) {
            setAlpha(0.7f);
        } else {
            setAlpha(1);
        }
    }

    @Override
    public void draw() {
        PApplet p = Services.processing;
        p.noStroke();
        if (isLocked || !isSolid()) {
            p.rect(0, 0, Config.Board.TILE_SIZE, Config.Board.TILE_SIZE);
        } else {
            p.fill(color);
            p.rect(0, 0, Config.Board.TILE_SIZE, Config.Board.TILE_SIZE);
        }
    }

}
