package gm.blockparty.tasks;

import gm.blockparty.Services;

public class RepeatingActionTask extends ActionTask {

    private float interval;
    private int numRepeats = Integer.MAX_VALUE;
    private float lastActionTime;
    private boolean actOnStart;

    public RepeatingActionTask(float interval, Action onActionHandler) {
        this(interval, false, onActionHandler);
    }

    public RepeatingActionTask(float interval, boolean actOnStart, Action onActionHandler) {
        super(onActionHandler);
        this.actOnStart = actOnStart;
        this.interval = interval;
    }

    public int getNumRepeats() {
        return numRepeats;
    }

    public void setNumRepeats(int numRepeats) {
        if (numRepeats < 0) {
            this.numRepeats = 0;
        } else {
            this.numRepeats = numRepeats;
        }
    }

    @Override
    protected void init() {
        if (actOnStart) {
            lastActionTime = -Float.MAX_VALUE;
        } else {
            lastActionTime = Services.time.now();
        }
    }

    @Override
    public void update() {
        float now = Services.time.now();
        float elapsedTime = now - lastActionTime;
        if (elapsedTime > interval) {
            lastActionTime = now;
            performAction();
            if (--numRepeats <= 0) {
                setStatus(Status.SUCCESS);
            }
        }
    }

}

