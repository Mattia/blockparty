package gm.blockparty.tasks;

import gm.blockparty.TransformObject;
import gm.util.Easing;
import gm.util.Util;

public class RotationTask extends TimedTask {

    float startRotation, targetRotation;
    Easing.Function easingFunction;
    TransformObject obj;

    public RotationTask(TransformObject obj, float duration, float targetRotation, Easing.Function easingFunction) {
        super(duration);
        this.obj = obj;
        this.startRotation = obj.transform.getRotation();
        this.targetRotation = targetRotation;
        this.easingFunction = easingFunction;
    }

    @Override
    public void update() {
        float t = getT();
        if (t >= 1) {
            obj.transform.setRotation(targetRotation);
            setStatus(Status.SUCCESS);
        } else {
            float a = Util.lerp(startRotation, targetRotation, easingFunction.call(t));
            obj.transform.setRotation(a);
        }
    }

    @Override
    protected void cleanUp() {
        this.obj = null;
    }
}
