package gm.blockparty.tasks;

import gm.blockparty.Services;
import gm.tasks.Task;

public class TimedTask extends Task {

    private final float duration;
    private float startTime;

    public TimedTask(float duration) {
        this.duration = duration;
    }

    @Override
    protected void _init() {
        startTime = Services.time.now();
        super._init();
    }

    protected float getT() {
        return getElapsedTime() / duration;
    }

    protected float getDuration() {
        return duration;
    }

    protected float getElapsedTime() {
        return (Services.time.now() - startTime);
    }

}
