package gm.blockparty.tasks;

import gm.blockparty.rendering.DrawableObject;
import gm.util.Easing;
import gm.util.Util;

public class AlphaTask extends TimedTask {

    private final float startAlpha;
    private final float endAlpha;
    private final Easing.Function easingFunction;
    private DrawableObject obj;

    public AlphaTask(DrawableObject obj, float duration) {
        this(obj, 1, 0, duration, Easing.Linear::easeNone);
    }

    public AlphaTask(DrawableObject obj, float startAlpha, float endAlpha, float duration, Easing.Function easingFunction) {
        super(duration);
        this.obj = obj;
        this.startAlpha = startAlpha;
        this.endAlpha = endAlpha;
        this.easingFunction = easingFunction;
    }

    @Override
    public void update() {
        float t = Util.clamp01(getT());
        float a = startAlpha + (endAlpha - startAlpha) * easingFunction.call(t);
        obj.setAlpha(a);
        if (t >= 1) {
            setStatus(Status.SUCCESS);
        }
    }

    @Override
    protected void cleanUp() {
        this.obj = null;
        super.cleanUp();
    }
}
