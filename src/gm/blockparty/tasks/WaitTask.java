package gm.blockparty.tasks;

public class WaitTask extends TimedTask {

    public WaitTask(float duration) {
        super(duration);
    }

    @Override
    public void update() {
        if (getT() >= 1) {
            setStatus(Status.SUCCESS);
        }
    }
}
