package gm.blockparty.tasks;

import gm.blockparty.rendering.DrawableObject;
import gm.util.Easing;
import gm.util.Util;

public class LerpColorTask extends TimedTask {

    private DrawableObject obj;
    private int startColor, endColor;
    private Easing.Function easingFunction;

    public LerpColorTask(DrawableObject obj, int startColor, int endColor, float duration, Easing.Function easingFunction) {
        super(duration);
        this.startColor = startColor;
        this.endColor = endColor;
        this.obj = obj;
        this.easingFunction = easingFunction;
    }

    @Override
    public void update() {
        float t = Util.clamp01(getT());
        if (t >= 1) {
            obj.setColor(endColor);
            setStatus(Status.SUCCESS);
        } else {
            int c = Util.lerpColor(startColor, endColor, easingFunction.call(t));
            obj.setColor(c);
        }
    }

    @Override
    protected void onAbort() {
        obj.setColor(startColor);
    }

    @Override
    protected void cleanUp() {
        this.obj = null;
        super.cleanUp();
    }
}
