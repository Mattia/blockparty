package gm.blockparty.tasks;

import gm.tasks.Task;

public abstract class ActionTask extends Task {

    private Action onActionHandler;

    public ActionTask(Action onActionHandler) {
        this.onActionHandler = onActionHandler;
    }

    final protected void performAction() {
        onActionHandler.execute();
    }

    public interface Action {
        void execute();
    }

}
