package gm.blockparty.tasks;

import gm.events.ClearEffectsEvent;
import gm.events.InfoTextLandedEvent;
import gm.blockparty.Config;
import gm.blockparty.Label;
import gm.blockparty.MainScene;
import gm.blockparty.Services;
import gm.tasks.Task;
import gm.util.Easing;
import gm.util.Util;

public class InfoTextEffect extends Label {

    private boolean isLanded;
    private boolean isBad;

    public static void clearAll() {
        Services.events.fire(new ClearEffectsEvent());
    }

    public InfoTextEffect(String s, boolean isBad) {
        super(Config.Render.Layer.UI_OVERLAY, isBad ? 0xFFFF0000 : 0xFFFFFFFF, Services.fonts.getDefaultFont(), 45, s);
        this.isBad = isBad;
    }

    @Override
    protected void init() {
        register(this::onClearEffects);
        register(this::onInfoTextArrived);

        final float duration = Config.Game.INFO_EFFECT_DURATION;

        MainScene scene = (MainScene)Services.scenes.getCurrentScene();

        // Move
        Util.Vec2f endPos = scene.getInfoBar().getInfoTextAnchor();
        Task moveTask = new MoveTask(this, endPos, duration/2, Easing.Quint::easeOut);
        moveTask.then(new ImmediateActionTask(()-> {
            this.isLanded = true;
            fireEvent(new InfoTextLandedEvent(this));
        }));
        addTask(moveTask);

        if (isBad) {
            addTask(new BlinkTask(this, duration, 8));
        } else {
            // Fade
            addTask(new AlphaTask(this, 0.2f, 1, duration/4, Easing.Quad::easeOut));// .then(new AlphaTask(effectLabel, 1, 0, duration/2, Easing.Quad::easeIn));
        }

        // Clear
        addTask(new DelayedActionTask(duration, this::destroy));

    }

    private void onClearEffects(ClearEffectsEvent e) {
        this.destroy();
    }

    private void onInfoTextArrived(InfoTextLandedEvent e) {
        if (e.effect != this && this.isLanded) {
            this.destroy();
        }
    }
}
