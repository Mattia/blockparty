package gm.blockparty.tasks;

public class ImmediateActionTask extends ActionTask {

    public ImmediateActionTask(Action onActionHandler) {
        super(onActionHandler);
    }

    @Override
    public void update() {
        performAction();
        setStatus(Status.SUCCESS);
    }
}
