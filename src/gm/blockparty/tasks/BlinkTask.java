package gm.blockparty.tasks;

import gm.blockparty.rendering.DrawableObject;
import gm.util.Easing;

public class BlinkTask extends TimedTask {

    DrawableObject obj;
    float period;

    public BlinkTask(DrawableObject obj, float duration, float period) {
        super(duration);
        this.obj = obj;
        this.period = period;
    }

    @Override
    public void update() {
        float t = getT();
        if (t >= 1) {
            obj.setAlpha(1);
            setStatus(Status.SUCCESS);
        } else {
            float a = (float)Math.abs(Math.sin(Easing.Quad.easeIn(t) * Math.PI * period));
            obj.setAlpha(a);
        }
    }
}
