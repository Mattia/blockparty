package gm.blockparty.tasks;

import gm.blockparty.Services;

public class DelayedActionTask extends ActionTask {

    private float startTime;
    private float duration;

    public DelayedActionTask(float duration, Action onActionHandler) {
        super(onActionHandler);
        this.duration = duration;
    }

    public float getDuration() {
        return duration;
    }

    @Override
    protected void init() {
        startTime = Services.time.now();
    }

    @Override
    public void update() {
        float elapsedTime = Services.time.now() - startTime;
        if (elapsedTime > duration) {
            setStatus(Status.SUCCESS);
        }
    }

    @Override
    protected void onSuccess() {
        performAction();
    }

}
