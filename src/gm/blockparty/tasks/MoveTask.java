package gm.blockparty.tasks;

import gm.blockparty.TransformObject;
import gm.util.Easing;
import gm.util.Util;

import static gm.util.Util.Vec2f;

public class MoveTask extends TimedTask {

    private final Vec2f startPos;
    private final Vec2f endPos;
    private final Easing.Function easingFunction;
    private TransformObject object;

    public MoveTask(TransformObject obj, Vec2f pos, float duration) {
        this(obj, pos, duration, Easing.Linear::easeNone);
    }

    public MoveTask(TransformObject obj, Vec2f pos, float duration, Easing.Function easingFunction) {
        super(duration);
        this.object = obj;
        this.startPos = obj.transform.getLocalPosition();
        this.endPos = new Vec2f(pos);
        this.easingFunction = easingFunction;
    }

    @Override
    public void update() {
        float t = easingFunction.call(Util.clamp01(getT()));

        float x = startPos.x + (endPos.x - startPos.x) * t;
        float y = startPos.y + (endPos.y - startPos.y) * t;
        object.transform.setLocalPosition(x, y);
        if (t >= 1) {
            setStatus(Status.SUCCESS);
        }
    }

    public Vec2f getStartPos() {
        return startPos;
    }

    public Vec2f getEndPos() {
        return endPos;
    }

    public TransformObject getObject() {
        return object;
    }

    @Override
    protected void cleanUp() {
        this.object = null;
        super.cleanUp();
    }

}
