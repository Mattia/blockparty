package gm.blockparty;

import gm.blockparty.rendering.DrawableObject;
import processing.core.PApplet;
import processing.core.PFont;

public class Label extends DrawableObject {

    private final int drawOrder;
    private String text;
    private float fontSize;
    private PFont font;
    private int horizontalAlignment;
    private int verticalAlignment;

    public Label() {
        this(Config.Render.Layer.UI_OVERLAY, Services.processing.color(255), Services.fonts.getDefaultFont(), Config.Render.DEFAULT_FONT_SIZE, "");
    }

    public Label(int drawOrder, int color, PFont font, float fontSize, String text) {
        this.drawOrder = drawOrder;
        this.color = color;
        this.font = font;
        this.fontSize = fontSize;
        this.text = text;
        horizontalAlignment = PApplet.CENTER;
        verticalAlignment = PApplet.CENTER;
    }

    public int getHorizontalAlignment() {
        return horizontalAlignment;
    }

    public void setHorizontalAlignment(int horizontalAlignment) {
        this.horizontalAlignment = horizontalAlignment;
    }

    public int getVerticalAlignment() {
        return verticalAlignment;
    }

    public void setVerticalAlignment(int verticalAlignment) {
        this.verticalAlignment = verticalAlignment;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public float getFontSize() {
        return fontSize;
    }

    public void setFontSize(float fontSize) {
        this.fontSize = fontSize;
    }

    public PFont getFont() {
        return font;
    }

    public void setFont(PFont font) {
        this.font = font;
    }

    @Override
    public int getDrawOrder() {
        return drawOrder;
    }

    @Override
    public void draw() {
        PApplet p = Services.processing;
        p.textAlign(horizontalAlignment, verticalAlignment);
        p.textFont(font, fontSize);
        p.text(text, 0, 0);
    }

}
