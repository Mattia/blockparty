package gm.blockparty;

import gm.events.GameOverEvent;

import java.util.ArrayList;

public class HighScoreManager {

    ArrayList<Integer> scores = new ArrayList<>();

    public HighScoreManager() {
        Services.events.register(this::onGameOver);
    }

    private void onGameOver(GameOverEvent e) {
        if (!scores.contains(e.finalScore)) {
            scores.add(e.finalScore);
        }
        scores.sort((s1, s2)-> s2 - s1);
    }

    public int getRankForScore(int score) {
        return scores.indexOf(score) + 1;
    }
}
