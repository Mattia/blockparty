package gm.blockparty;

import gm.events.PlayerMoveChangedEvent;
import gm.events.StepEvent;
import gm.events.TetrominoLockedEvent;
import gm.events.TickEvent;
import gm.blockparty.tasks.LerpColorTask;
import gm.util.Easing;
import gm.util.FSM;
import gm.util.Util;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

import static gm.blockparty.Move.*;
import static gm.util.Util.Vec2i;

public class Tetromino extends Piece<TetrominoTile> {

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// INTERNAL CLASSES
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	private class ButtonState {
		private boolean isPressed;
		private float timePressed;

		public void setPressed(boolean state) {
			isPressed = state;
			if (state) {
				timePressed = Services.time.now();
			}
		}

		public float getPressedDuration() {
			if (isPressed) {
				return Services.time.now() - timePressed;
			} else {
				return 0;
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// VARS
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	static final private Move[] REPEATABLE_MOVES = {DOWN, LEFT, RIGHT};

	private Player player;
	private FSM states;
	private HashMap<Move, ButtonState> buttonStates;
	private Queue<Move> pendingMoves;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// LIFECYCLE
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	Tetromino(Player player, Shape shape) {
		super(shape, new Grid.Transform(new Vec2i(player.getStartPosition(), 0)), TetrominoTile.class);

		buttonStates = new HashMap<>();
		buttonStates.put(LEFT, new ButtonState());
		buttonStates.put(RIGHT, new ButtonState());
		buttonStates.put(DOWN, new ButtonState());
		buttonStates.put(ROTATE, new ButtonState());

		pendingMoves = new LinkedList<>();

		for (TetrominoTile tile : getTiles()) {
			tile.setColor(player.getColor());
		}

		// attach the number label to the "center" tile
		int centerTileIndex = tiles.size() / 2;
		TetrominoTile tile = getTiles().get(centerTileIndex);
		int number = player.getDisplayNumber();
		TileNumberLabel numberLabel = new TileNumberLabel(number);
		tile.addChild(numberLabel);
		numberLabel.transform.setParent(tile.transform);
		numberLabel.setColor(Util.getComplementary(player.getColor()));
		this.player = player;

		states = new FSM(this);
		states.addState(new Appearing());
		states.addState(new Moving());
		states.addState(new Locking());
		states.addState(new Locked());
		states.transitionTo(Appearing.class);
	}

	@Override
	protected void init() {
		register(this::onTick);
		register(this::onStep);
		register(this::onPlayerMove);
	}

	@Override
	public void onDestroy() {
		if (states.getCurrentState().getClass() != Locked.class) {
			tiles.forEach(Tile::destroy);
		}
		tiles.clear();
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// EVENT HANDLERS
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	private void onPlayerMove(PlayerMoveChangedEvent e) {
		if (e.playerId == player.getId()) {
			ButtonState state = buttonStates.get(e.move);
			state.setPressed(e.active);
			if (e.active) {
				pendingMoves.add(e.move);
			}
		}
	}

	private void onTick(TickEvent e) {
		if (isAlive()) ((TetrominoState)states.getCurrentState()).onTick();
	}

	private void onStep(StepEvent e) {
		if (isAlive()) ((TetrominoState)states.getCurrentState()).onStep();
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// MOVEMENT
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	private boolean move(Move move) {
		Board board = Services.game.getBoard();
		return board.moveTetromino(this, move);
	}

	private boolean step() {
		Board board = Services.game.getBoard();
		return board.moveTetromino(this, DOWN);
	}

	public void lockTiles() {
		for (TetrominoTile tile : tiles) {
			tile.setLocked(true);
		}
	}

	public boolean isIntersectingLockedTetromino() {
		for (Tile myTile : tiles) {
			for (Tile cellTile : myTile.getCell().getTiles()) {
				if (cellTile.isLocked() && (cellTile instanceof TetrominoTile)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean isStopped() {
		return Services.game.getBoard().isTetronimoStopped(this);
	}

	public void playPlacementEffect() {
		Services.sounds.playPiecePlacedInside();
		for (Tile t : tiles) {
			t.addTask(new LerpColorTask(t, 0xFFFFFFFF, t.getColor(), 0.75f, Easing.Bounce::easeIn));
		}
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// STATES
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	interface MoveHandler {
		void handle(Move m);
	}

	private class TetrominoState extends FSM.State {
		void onTick() {}
		void onStep() {}

		void processMoves(MoveHandler handler) {

			while (!pendingMoves.isEmpty()) {
				Move m = pendingMoves.remove();
				handler.handle(m);
				if (!isAlive()) return;
			}

			for (Move moveType : REPEATABLE_MOVES) {
				if (!isAlive()) return;
				ButtonState button = buttonStates.get(moveType);
				if (button.getPressedDuration() > Config.Timing.AUTO_REPEAT_DELAY) {
					handler.handle(moveType);
				}
			}
		}
	}

	private class Appearing extends TetrominoState {

		private void setTilesMoveFlag() {
			tiles.forEach(t->t.setDidMove(true));
		}

		@Override
		void onTick() {
			processMoves(m->{
				if (move(m)) {
					setTilesMoveFlag();
					transitionTo(Moving.class);
				}
			});
		}

		@Override
		void onStep() {
			if (step()) {
				setTilesMoveFlag();
				transitionTo(Moving.class);
			}
		}
	}

	private class Moving extends TetrominoState {
		@Override
		void onTick() {
			processMoves(Tetromino.this::move);
			if (isStopped()) {
				transitionTo(Locking.class);
			}
		}

		@Override
		void onStep() {
			step();
		}
	}

	private class Locking extends TetrominoState {
		private float lockingStart;
		@Override
		protected void onEnter(FSM.State previousState) {
			lockingStart = Services.time.now();
		}

		@Override
		void onTick() {
			float timeSinceStart = Services.time.now() - lockingStart;
			processMoves(m->{
				if (move(m)) {
					if (m == DOWN) {
						transitionTo(Moving.class);
					}
				} else if (m == DOWN && timeSinceStart >= Config.Timing.FORCE_LOCK_DELAY) {
					transitionTo(Locked.class);
				}
			});
			if (timeSinceStart >= Config.Timing.LOCK_DELAY && isStopped()) {
				transitionTo(Locked.class);
			}
		}

		@Override
		void onStep() {
			if (step()) {
				transitionTo(Moving.class);
			}
		}
	}

	private class Locked extends TetrominoState {
		@Override
		protected void onEnter(FSM.State previousState) {
			Services.sounds.playPlayerTone(player.getDisplayNumber()-1);
			pendingMoves.clear();
			fireEvent(new TetrominoLockedEvent(Tetromino.this));
			lockTiles();
			player.clearTetromino();
			destroy();
		}
	}

}