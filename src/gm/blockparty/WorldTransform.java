package gm.blockparty;

import java.util.ArrayList;

import static gm.util.Util.Vec2f;

public class WorldTransform {

    private WorldTransform parent;
    private final ArrayList<WorldTransform> children;
    private final Vec2f localPosition;
    private final Vec2f position;
    private float rotation;

    public WorldTransform() {
        children = new ArrayList<>();
        localPosition = new Vec2f();
        position = new Vec2f();
    }

    public WorldTransform getParent() {
        return parent;
    }

    public int numDescendants() {
        int n = children.size();
        for (WorldTransform t : children) {
            n += t.numDescendants();
        }
        return n;
    }

    public void setParent(WorldTransform parent) {
        if (this.parent != null) {
            this.parent.removeChild(this);
        }
        this.parent = parent;
        if (this.parent != null) {
            this.parent.addChild(this);
        }
        this.update();
    }

    public Vec2f getLocalPosition() {
        return new Vec2f(localPosition);
    }

    public void setLocalPosition(Vec2f pos) {
        localPosition.x = pos.x;
        localPosition.y = pos.y;
        update();
    }

    public void setLocalPosition(float x, float y) {
        localPosition.x = x;
        localPosition.y = y;
        update();
    }

    public Vec2f getPosition() {
        return new Vec2f(position);
    }

    public float getRotation() {
        return rotation;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    public WorldTransform move(float x, float y) {
        localPosition.x += x;
        localPosition.y += y;
        update();
        return this;
    }

    private void addChild(WorldTransform child) {
        children.add(child);
        child.update();
    }

    private void removeChild(WorldTransform child) {
        children.remove(child);
    }

    public void update() {
        if (parent != null) {
            Vec2f parentPosition = parent.getPosition();
            position.x = parentPosition.x + localPosition.x;
            position.y = parentPosition.y + localPosition.y;
        } else {
            position.x = localPosition.x;
            position.y = localPosition.y;
        }
        children.forEach(WorldTransform::update);
    }

    void clearParent() {
        if (parent != null) {
            parent.removeChild(this);
            parent = null;
        }
    }

    public ArrayList<WorldTransform> getChildren() {
        return children;
    }
}
