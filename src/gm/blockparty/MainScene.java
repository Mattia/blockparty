package gm.blockparty;

import gm.events.GameResetEvent;
import gm.events.KeyEvent;
import gm.scenes.Scene;

public class MainScene extends Scene {

    private UrlBar urlBar;
    private InfoBar infoBar;

    @Override
    protected void init() {

        final int bw = Config.Board.WIDTH;
        final int bh = Config.Board.HEIGHT;
        final float ts = Config.Board.TILE_SIZE;
        final float hts = Config.Board.TILE_SIZE / 2.0f;

        Board board = new Board(bw, bh);
        board.transform.setParent(getRootTransform());

        // Move the board to allow for proper rendering of centered tiles
        board.transform.move(hts, hts);
        board.transform.move(0, (Config.Board.URL_BAR_HEIGHT + Config.Board.INFO_BAR_HEIGHT) * ts);
        Services.game = new Game(board);

        urlBar = new UrlBar();
        urlBar.transform.move(ts * bw / 2, Config.Board.URL_BAR_HEIGHT / 2 * ts + hts);
        urlBar.transform.setParent(getRootTransform());

        infoBar = new InfoBar();
        infoBar.transform.move(ts * bw / 2, (Config.Board.INFO_BAR_HEIGHT / 2 * ts) + Config.Board.URL_BAR_HEIGHT * ts + hts);

        Services.events.register(this::onKey);
    }

    public InfoBar getInfoBar() {
        return infoBar;
    }


    private void onKey(KeyEvent e) {
        if (e.key == 'r' && e.isDown) {
            Services.events.queueEvent(new GameResetEvent());
        }
    }

    @Override
    protected void onDestroy() {
        urlBar.destroy();
        infoBar.destroy();
    }
}
