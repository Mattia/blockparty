package gm.blockparty.rendering;

import gm.blockparty.Config;
import gm.blockparty.Services;
import gm.blockparty.TransformObject;
import gm.util.GameObject;
import gm.util.Util;

public class DrawableObject extends TransformObject implements Drawable {

    private boolean isVisible = false;
    protected int color;
    protected float alpha = 1;

    @Override
    public void _init() {
        setVisible(true);
        super._init();
    }

    @Override
    public void _destroy() {
        setVisible(false);
        super._destroy();
        transform.setParent(null);
    }

    public void setVisible(boolean isVisible) {
        if (!isAlive()) return;

        if (this.isVisible == isVisible) return;
        if (isVisible) {
            Services.renderer.registerDrawable(this);
        } else {
            Services.renderer.unregisterDrawable(this);
        }

        this.isVisible = isVisible;

        //noinspection Convert2streamapi
        for (GameObject c : getChildren()) {
            if (c instanceof DrawableObject) {
                ((DrawableObject) c).setVisible(this.isVisible);
            }
        }
    }

    @Override
    public int getDrawOrder() {
        return Config.Render.Layer.DEFAULT;
    }

    @Override
    public void draw() {}

    public int getColor() {
        return Util.setAlpha(color, (int)(getAlpha() * 255));
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }

    public float getAlpha() {
        return alpha;
    }
}
