package gm.blockparty.rendering;

public interface Drawable {
    default int getDrawOrder() { return 0; }
    void draw();
}
