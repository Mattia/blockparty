package gm.blockparty.rendering;

import gm.blockparty.Config;
import gm.blockparty.Services;
import gm.util.Util;
import processing.core.PApplet;

import java.util.ArrayList;
import java.util.Collections;

public class Renderer {

    private boolean needsSorting;
    private ArrayList<DrawableObject> drawables;

    public Renderer() {
        drawables = new ArrayList<>();
    }

    public void registerDrawable(DrawableObject drawable) {
        drawables.add(drawable);
        needsSorting = true;
    }

    public void unregisterDrawable(DrawableObject drawable) {
        drawables.remove(drawable);
    }

    float t = 0;

    public void draw() {
        if (needsSorting) {
            Collections.sort(drawables, (DrawableObject d1, DrawableObject d2)-> d1.getDrawOrder() -  d2.getDrawOrder());
            needsSorting = false;
        }
        PApplet p = Services.processing;

        p.background(0);
        p.rectMode(PApplet.CENTER);
        p.smooth();

        p.pushMatrix();
        p.scale(Config.Window.SCALE);
        for (DrawableObject o : drawables) {
            if (!o.isAlive()) {
                System.out.println("WHAT?!");
            }
            p.pushMatrix();
            Util.Vec2f pos = o.transform.getPosition();
            p.translate(pos.x, pos.y);
            p.rotate(o.transform.getRotation());
            p.fill(o.getColor());
            o.draw();
            p.popMatrix();
        }
        p.popMatrix();
    }

}
