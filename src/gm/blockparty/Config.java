package gm.blockparty;

public class Config {

	public static class Window {
		public final static boolean IS_FULLSCREEN = true;
		public final static float SCALE = 1.0f;
		public final static int WIDTH = 1280;
		public final static int HEIGHT = 720;
	}

	public static class Timing {
		public final static float TICK_INTERVAL = 0.035f;
		public final static int TICKS_PER_STEP = 45;

		public final static float LOCK_DELAY = 0.75f; //
		public final static float FORCE_LOCK_DELAY = 0.15f; // number of ticks to wait to apply force lock when pushing down
		public final static float AUTO_REPEAT_DELAY = 0.25f; // number of ticks to wait to before "repeating" when the player has a button pressed
	}

	public static class Testing {
        public final static boolean IS_TESTING = false;
        public static final int NUM_TEST_CLIENTS = 16;
        public final static float UPDATE_INTERVAL = 0.75f;

        public final static int DO_NOTHING_WEIGHT =     50;
        public final static int JOIN_REQUEST_WEIGHT =   10;
        public final static int MOVE_WEIGHT =           200;
        public final static int DISCONNECT_WEIGHT =     1;

		public final static boolean ACTIVATE_SOUNDS = true;
    }

	public static class Board {
		public static final int PLAYER_SLOT_WIDTH = 4;
		public final static int URL_BAR_HEIGHT = 3;
		public final static int INFO_BAR_HEIGHT = 5;
		public final static int WIDTH = 64;
		public final static int HEIGHT = 32;
		public final static float TILE_SIZE = (float)Window.WIDTH / WIDTH;
		public final static int CLOSED_TILE_TINT = 0xFFFFFFFF;
        public final static int OPEN_TILE_TINT = 0xFF000088;
	}

	public static class Game {
		public static final int MAX_PLAYERS = Board.WIDTH / Board.PLAYER_SLOT_WIDTH;
		public static final float GAME_DURATION = 120; // in seconds
		public static final float GAME_START_COUNTDOWN_DURATION = 30; // in seconds
        public static final float GAME_ENDING_COUNTDOWN_DURATION = 30; // in seconds
        public static final float GAME_OVER_SCREEN_DURATION = 5; // in seconds
		public static final float INFO_EFFECT_DURATION = 4.0f;
	}

	public static class BonusPiece {
		public static final float BONUS_CREATION_INTERVAL = 0.5f; // in seconds
		public static final int NUM_ON_SCREEN_BONUS_PIECES = 5;
	}

	public static class Score {
		public static final int POINTS_PER_BONUS_TILE = 3;
		public static final double COMPLEXITY_EXPONENT = 2;
	}

	public static class Network {
		public static final String CLIENT_URL = "blockrock.me";
		public static final String ROUTER_URL = "ws://" + CLIENT_URL + ":80";
		// public static final String ROUTER_URL = "ws://localhost:5000";
	}

	public static class Render {
		public static final float DEFAULT_FONT_SIZE = 24;
		public static final int BAR_COLOR = 0xFF252a3a;

		public static int[] PLAYER_COLORS = {
				0xFFB72121,
				0xFFF66F00,
				0xFFD0E900,
				0xFF15D000,
				0xFFFFA6F2,
				0xFF0DFCAB,
				0xFFE7C865,
				0xFF85F600,
				0xFFD52EEC,
				0xFF9AD160,
				0xFFFFCE1C,
				0xFF00DA85,
				0xFFF31707,
				0xFFFF52D5,
				0xFFBE672C,
				0xFFC51258
		};

		public static class Layer {
			public final static int BACKGROUND = 10;
			public final static int SCORING_TILE = 50;
			public final static int TETROMINO_TILE = 100;
			public final static int UI_BASE = 200;
			public final static int UI_OVERLAY = 200;
			public static final int DEFAULT = 0;
		}
	}
}