package gm.tasks;

import java.util.ArrayList;
import java.util.List;

public class TaskManager {
    private List<Task> _tasks = new ArrayList<>();

    public boolean hasTasks() {
        return _tasks.size() > 0;
    }

    public void addTask(Task task) {
        assert (task != null);
        assert (!task.isAttached());
        _tasks.add(task);
        task.setStatus(Task.Status.PENDING);
    }

    public void abortAllTasks() {
        _tasks.forEach(Task::abort);
    }

    public void update() {
        for (int i = _tasks.size() - 1; i >= 0; i--) {
            Task t = _tasks.get(i);
            if (t.isPending()) {
                t.setStatus(Task.Status.WORKING);
            }
        }

        for (int i = _tasks.size() - 1; i >= 0; i--) {
            Task t = _tasks.get(i);
            if (t.isWorking()) {
                t._update();
            }
        }

        for (int i = _tasks.size() - 1; i >= 0; i--) {
            Task t = _tasks.get(i);
            if (t.isFinished()) {
                handleCompletion(t);
            }
        }
    }

    private void handleCompletion(Task t) {
        if (t.getNextTask() != null && t.isSuccessful()) {
            addTask(t.getNextTask());
        }
        _tasks.remove(t);
        t.setStatus(Task.Status.DETACHED);
    }

}