package gm.tasks;

public abstract class Task {

    private Status status = Task.Status.DETACHED;
    private Task nextTask;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status newStatus) {
        if (status == newStatus) return;

        switch (newStatus) {
            case WORKING:
                _init();
                break;
            case SUCCESS:
                    onSuccess();
                    cleanUp();
                break;
            case ABORTED:
                    if (isAttached()) {
                        onAbort();
                    }
                    cleanUp();
                break;
            case FAIL:
                    onFail();
                    cleanUp();
                break;
            case DETACHED:
            case PENDING:
                break;
            default:
                throw new IllegalArgumentException();
        }
        status = newStatus;
    }

    // Convenience status checking
    public boolean isDetached() {
        return !isAttached();
    }

    public boolean isAttached() {
        return status == Status.WORKING || status == Status.PENDING;
    }

    public boolean isPending() {
        return status == Status.PENDING;
    }

    public boolean isWorking() {
        return status == Status.WORKING;
    }

    public boolean isSuccessful() {
        return status == Status.SUCCESS;
    }

    public boolean isFailed() {
        return status == Status.FAIL;
    }

    public boolean isAborted() {
        return status == Status.ABORTED;
    }

    public boolean isFinished() {
        return status == Status.FAIL || status == Status.SUCCESS || status == Status.ABORTED;
    }

    // Convenience method for external classes to abort the task
    public void abort() {
        setStatus(Status.ABORTED);
        Task next = nextTask;
        while(next != null && !next.isAborted()) {
            next.abort();
            next = next.getNextTask();
        }
    }

    // Subclasses can override these to respond to status changes
    protected void onAbort() {}

    protected void onSuccess() {}

    protected void onFail() {}

    protected void _init() {
        init();
    }

    protected void _update() {
        update();
    }

    // Override this to handle initialization of the task.
    // This is called when the task enters the WORKING status
    protected void init() {
    }

    public void update() {
    }

    // This is called when the tasks completes (i.e. is aborted,
    // fails, or succeeds). It is called after the status change
    // handlers are called
    protected void cleanUp() {
    }

    public Task getNextTask() {
        return nextTask;
    }

    // Sets a task to be automatically attached when this one completes successfully
    // NOTE: if a task is aborted or fails, its next task will not be queued
    // NOTE: **DO NOT** assign attached tasks with this method.
    public Task then(Task task) {
        assert (!task.isAttached());
        Task root = this;
        while (root.nextTask != null) root = root.nextTask;
        root.nextTask = task;
        return task;
    }

    public enum Status {
        DETACHED, // Task has not been attached to a TaskManager
        PENDING, // Task has not been initialized
        WORKING, // Task has been initialized and has started receiving updates
        SUCCESS, // Task completed successfully
        FAIL, // Task completed unsucessfully
        ABORTED // Task was aborted
    }

}