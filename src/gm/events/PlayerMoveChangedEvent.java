package gm.events;

import gm.blockparty.Move;
import gm.util.Event;

public class PlayerMoveChangedEvent extends Event {
    public final Move move;
    public final long playerId;
    public final boolean active;

    public PlayerMoveChangedEvent(Move move, long playerId, boolean active) {
        this.move = move;
        this.playerId = playerId;
        this.active = active;
    }
}
