package gm.events;

import gm.util.Event;

public class ClientDisconnectedEvent extends Event {
    public final long id;

    public ClientDisconnectedEvent(long id) {
        this.id = id;
    }
}
