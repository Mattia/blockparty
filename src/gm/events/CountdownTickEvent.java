package gm.events;

import gm.util.Event;

public class CountdownTickEvent extends Event {
    public final int timeRemaining;

    public CountdownTickEvent(int timeRemaining) {
        this.timeRemaining = timeRemaining;
    }
}
