package gm.events;

import gm.blockparty.tasks.InfoTextEffect;
import gm.util.Event;

public class InfoTextLandedEvent extends Event {
    final public InfoTextEffect effect;

    public InfoTextLandedEvent(InfoTextEffect effect) {
        this.effect = effect;
    }
}
