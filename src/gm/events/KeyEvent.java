package gm.events;

public class KeyEvent extends gm.util.Event {
    public final char key;
    public final boolean isDown;

    public KeyEvent(char key, boolean isDown) {
        this.key = key;
        this.isDown = isDown;
    }
}
