package gm.events;

import gm.util.Event;

public class GameOverEvent extends Event {
    public final int finalScore;

    public GameOverEvent(int finalScore) {
        this.finalScore = finalScore;
    }
}
