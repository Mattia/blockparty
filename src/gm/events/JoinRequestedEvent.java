package gm.events;

import gm.util.Event;

public class JoinRequestedEvent extends Event {
    final public long id;

    public JoinRequestedEvent(long id) {
        this.id = id;
    }
}
