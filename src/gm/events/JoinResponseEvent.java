package gm.events;

import gm.util.Event;

public class JoinResponseEvent extends Event {
    final public long id;
    final public boolean success;
    final public int color;
    final public int displayNumber;

    public JoinResponseEvent(long id, boolean success, int color, int displayNumber) {
        this.id = id;
        this.success = success;
        this.color = color;
        this.displayNumber = displayNumber;
    }

}
