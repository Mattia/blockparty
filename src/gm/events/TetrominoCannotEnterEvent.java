package gm.events;

import gm.blockparty.Tetromino;
import gm.util.Event;

public class TetrominoCannotEnterEvent extends Event {
    public final Tetromino tetromino;

    public TetrominoCannotEnterEvent(Tetromino tetromino) {
        this.tetromino = tetromino;
    }
}
