package gm.events;

import gm.util.Event;

public class PlayerJoinedEvent extends Event {
    public final long id;

    public PlayerJoinedEvent(long id) {
        this.id = id;
    }
}
