package gm.events;

import gm.util.Event;

public class PointsScoredEvent extends Event {
    public final int currentScore;
    public final int scoreDelta;

    public PointsScoredEvent(int currentScore, int scoreDelta) {
        this.currentScore = currentScore;
        this.scoreDelta = scoreDelta;
    }
}
