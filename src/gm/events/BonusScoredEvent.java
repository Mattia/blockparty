package gm.events;

import gm.blockparty.BonusPiece;
import gm.util.Event;

public class BonusScoredEvent extends Event {
    public final BonusPiece piece;

    public BonusScoredEvent(BonusPiece piece) {
        this.piece = piece;
    }
}
