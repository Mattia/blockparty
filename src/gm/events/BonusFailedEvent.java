package gm.events;

import gm.blockparty.BonusPiece;
import gm.util.Event;

public class BonusFailedEvent extends Event {
    public final BonusPiece piece;

    public BonusFailedEvent(BonusPiece piece) {
        this.piece = piece;
    }
}
