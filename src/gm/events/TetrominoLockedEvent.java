package gm.events;

import gm.blockparty.Tetromino;
import gm.util.Event;

public class TetrominoLockedEvent extends Event {
    public final Tetromino tetromino;

    public TetrominoLockedEvent(Tetromino tetromino) {
        this.tetromino = tetromino;
    }
}
