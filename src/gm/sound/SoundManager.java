package gm.sound;

import gm.blockparty.Config;
import gm.blockparty.Services;
import gm.util.Util;
import processing.sound.SoundFile;

import java.io.File;
import java.util.HashMap;

public class SoundManager {

    static private final String SOUNDS_PATH = "sounds/";

    HashMap<String, SoundFile> sounds;

    public SoundManager() {
        sounds = new HashMap<>();
        if (!Config.Testing.ACTIVATE_SOUNDS) return;

        String dataPath = Services.processing.dataPath("");
        File dir = new File(dataPath + "/" + SOUNDS_PATH);
        File[] files = dir.listFiles();

        if (files == null) {
            Util.log("No files found in sounds directory");
        } else {
            for (File file : files) {
                String fileName = file.getName();
                int i = fileName.lastIndexOf(".");
                if (i > 0) {
                    String fileExtension = fileName.substring(i + 1);
                    if (isSupportedFileType(fileExtension)) {
                        SoundFile sound = new SoundFile(Services.processing, SOUNDS_PATH + fileName);
                        sounds.put(fileName.substring(0, i), sound);
                    }
                }
            }
        }
    }

    static String[] playerToneNames = {
            "Player01",
            "Player02",
            "Player03",
            "Player04",
            "Player05",
            "Player06",
            "Player07",
            "Player08",
            "Player09",
            "Player10",
            "Player11",
            "Player12",
            "Player13",
            "Player14",
            "Player15",
            "Player16"
    };

    public void playPlayerTone(int i) {
        if (!Config.Testing.ACTIVATE_SOUNDS) return;
        assert(i >= 0);
        assert(i < playerToneNames.length);

        sounds.get(playerToneNames[i]).play(1, 0.7f);
    }

    public void playGameMusic() {
        if (!Config.Testing.ACTIVATE_SOUNDS) return;
        sounds.get("Block Party").play();
    }

    public void stopGameMusic() {
        if (!Config.Testing.ACTIVATE_SOUNDS) return;
        sounds.get("Block Party").stop();
    }

    public void playStartCountdown() {
        if (!Config.Testing.ACTIVATE_SOUNDS) return;
        sounds.get("StartCountdown").play(1, 0.15f);
    }

    public void stopStartCountdown() {
        if (!Config.Testing.ACTIVATE_SOUNDS) return;
        sounds.get("StartCountdown").stop();
    }

    public void playBonusAppeared() {
        if (!Config.Testing.ACTIVATE_SOUNDS) return;
        sounds.get("BlockAppearHiLoud").play();
    }

    public void playPiecePlacedInside() {
        if (!Config.Testing.ACTIVATE_SOUNDS) return;
        sounds.get("ClearB").play();
    }

    public void playBonusScored() {
        if (!Config.Testing.ACTIVATE_SOUNDS) return;
        sounds.get("ClearA").play();
    }

    public void playBonusBroken() {
        if (!Config.Testing.ACTIVATE_SOUNDS) return;
        sounds.get("BlockBreak2C").play(1, 0.5f);
    }

    public void playCountdownTick() {
        if (!Config.Testing.ACTIVATE_SOUNDS) return;
        sounds.get("CountdownDoubleFastLow").play();
    }

    public void playGameOver() {
        if (!Config.Testing.ACTIVATE_SOUNDS) return;
        sounds.get("GameOver").play(1, 0.75f);
    }

    private boolean isSupportedFileType(String extension) {
        return extension.equals("mp3") || extension.equals("wav") || extension.equals("ogg");
    }

}