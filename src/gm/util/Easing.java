package gm.util;

public class Easing {

    public interface Function {
        float call(float t);
    }

    public static class Linear {
        public static float easeNone(float t) {
            return t;
        }
    }

    public static class Sine {

    }

    public static class Quad {
        public static float easeIn(float t) {
            return t * t;
        }

        public static float easeOut(float t) {
            return -t * (t - 2);
        }
    }

    public static class Quint {
        public static float easeIn(float t) {
            return t * t * t * t * t;
        }

        public static float easeOut(float t) {
            t -= 1;
            return t * t * t * t * t + 1;
        }
    }

    public static class Bounce {
        public static float easeIn(float t) {
            return 1 - easeOut(1 - t);
        }

        public static float easeOut(float t) {
            if (t < (1 / 2.75f))
                return (7.5625f * t * t);
            else if (t < (2 / 2.75f))
                return (7.5625f * (t -= (1.5f / 2.75f)) * t + .75f);
            else if (t < (2.5 / 2.75f))
                return (7.5625f * (t -= (2.25f / 2.75f)) * t + .9375f);
            else
                return (7.5625f * (t -= (2.625f / 2.75f)) * t + .984375f);
        }
    }


    public static class Elastic {
        public static float easeIn(float t) {
            if (t == 1) return 1;

            float p = 0.3f;
            float s = p / 4;

            return (float) -(Math.pow(2, 10 * (t -= 1)) * Math.sin((t - s) * (2 * Math.PI) / p));
        }

        public static float easeOut(float t) {
            if (t == 1) return 1;

            float p = 0.3f;
            float s = p / 4;

            return (float) (Math.pow(2, -10 * t) * Math.sin((t - s) * (2 * Math.PI) / p) + 1);
        }
    }

}
