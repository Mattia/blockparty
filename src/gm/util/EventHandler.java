package gm.util;

public interface EventHandler<EVENT_TYPE> {
	void handle(EVENT_TYPE e);
}
