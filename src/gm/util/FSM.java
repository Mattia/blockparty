package gm.util;

import java.util.HashMap;

public class FSM {
    private Object context;

    private HashMap<Class<? extends State>, State> states = new HashMap<>();

    public void addState(State state) {
        state.setParent(this);
        states.put(state.getClass(), state);
    }

    private State currentState;
    public State getCurrentState() {
        return currentState;
    }

    public void update() {
        if (currentState != null) {
            currentState.update();
        }
    }

    public FSM(Object context) {
        this.context = context;
    }

    public Object getContext() {
        return context;
    }

    public void transitionTo(Class<? extends State> stateType) {
        if (currentState != null && currentState.getClass() == stateType) {
            return;
        }
        State nextState = states.get(stateType);
        assert nextState != null;

        if (!nextState.isInitialized) {
            nextState.init();
            nextState.isInitialized = true;
        }

        State previousState = currentState;
        if (previousState != null) {
            previousState.onExit(nextState);
        }
        currentState = nextState;
        nextState.onEnter(previousState);
    }

    public static class State {

        private boolean isInitialized = false;

        private FSM parent;

        public void setParent(FSM parent) {
            this.parent = parent;
        }

        protected Object getContext() {
            return parent.getContext();
        }

        protected final void transitionTo(Class<? extends State> stateType) {
            parent.transitionTo(stateType);
        }

        protected void onEnter(State previousState) {}
        protected void onExit(State nextState) {}

        protected void init() {}
        protected void update() {}
        protected void cleanUp() {}
    }

}
