package gm.util;

import net.jodah.typetools.TypeResolver;

import java.util.ArrayList;
import java.util.HashMap;

public class EventManager {

    private static int nextEntryIndex = Integer.MIN_VALUE;
    private HashMap<Class, ArrayList<HandlerEntry>> handlers;
    private ArrayList<Event> eventQueue;

    public EventManager() {
        handlers = new HashMap<>();
        eventQueue = new ArrayList<>();
    }

    public <T extends Event> EntryKey register(EventHandler<T> handler) {
        Class c = TypeResolver.resolveRawArgument(EventHandler.class, handler.getClass());
        //noinspection unchecked
        return register(c, handler);
    }

    synchronized public <T extends Event> EntryKey register(Class<? extends Event> eventType, EventHandler<T> handler) {
        HandlerEntry entry = new HandlerEntry();
        entry.handler = handler;
        entry.key = new EntryKey();
        entry.key.index = nextEntryIndex++;
        entry.key.eventType = eventType;
        ArrayList<HandlerEntry> entries = entriesForEvent(eventType);
        entries.add(entry);
        return entry.key;
    }

    synchronized public void unregister(EntryKey key) {
        ArrayList<HandlerEntry> entries = entriesForEvent(key.eventType);
        for (int i = 0; i < entries.size(); i++) {
            HandlerEntry entry = entries.get(i);
            if (entry.key.index == key.index) {
                entries.remove(i);
                break;
            }
        }
    }

    private ArrayList<HandlerEntry> entriesForEvent(Class<? extends Event> eventType) {
        ArrayList<HandlerEntry> entries;
        if (handlers.containsKey(eventType)) {
            entries = handlers.get(eventType);
        } else {
            entries = new ArrayList<>();
            handlers.put(eventType, entries);
        }
        return entries;
    }

    synchronized public void fire(Event e) {
        Class c = e.getClass();
        // System.out.println("FIRING: " + c.getName());
        if (handlers.containsKey(c)) {
            ArrayList<HandlerEntry> handlerList = handlers.get(c);
            for (int i = handlerList.size() - 1; i >= 0; --i) {
                HandlerEntry entry = handlerList.get(i);
                //noinspection unchecked
                entry.handler.handle(e);
            }
        }
    }

    synchronized public void processQueuedEvents() {
        for (int i = 0; i < eventQueue.size(); i++) {
            fire(eventQueue.get(i));
        }
        eventQueue.clear();
    }

    synchronized public void queueEvent(Event e) {
        eventQueue.add(e);
    }

    public class EntryKey {
        public Class<? extends Event> eventType;
        public int index;
    }

    private class HandlerEntry {
        public EventHandler handler;
        public EntryKey key;

        @Override
        public int hashCode() {
            return key.index;
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof HandlerEntry && ((HandlerEntry) obj).key.index == key.index;
        }
    }

}
