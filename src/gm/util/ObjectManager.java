package gm.util;

import java.util.ArrayList;

public class ObjectManager {

    private ArrayList<GameObject> objects = new ArrayList<>();
    private ArrayList<GameObject> newObjects = new ArrayList<>();

    private boolean needsSorting = false;

    public void update() {
        addNewObjects();
        sortObjects();

        objects.forEach(GameObject::_preUpdate);
        objects.forEach(GameObject::_update);
        objects.forEach(GameObject::_postUpdate);

        objects.stream().filter(o->!o.isAlive()).forEach(GameObject::_cleanUp);
        objects.removeIf(o->!o.isAlive());
    }

    private void sortObjects() {
        if (needsSorting) {
            objects.sort((o1, o2) -> o1.getPriority() - o2.getPriority());
        }
        needsSorting = false;
    }

    private void addNewObjects() {
        for (int i = 0; i < newObjects.size(); i++) {
            newObjects.get(i)._init();
        }
        objects.addAll(newObjects);
        newObjects.clear();
    }

    public void add(GameObject object) {
        newObjects.add(object);
        needsSorting = true;
    }

    public void clear() {
        objects.addAll(newObjects);
        newObjects.clear();
        objects.forEach(GameObject::destroy);
        objects.forEach(GameObject::_cleanUp);
        objects.clear();
    }

    public int getNumObjects() {
        return objects.size();
    }

    // TODO: DELETE ME WHEN DONE DEBUGGING
    public ArrayList<GameObject> getObjects() {
        return objects;
    }

    public <T extends GameObject> ArrayList<T> getObjectsOfType(Class<T> type) {
        ArrayList<T> result = new ArrayList<>();
        getObjects().forEach(o-> {
            if (type.isInstance(o)) {
                result.add(type.cast(o));
            }
        });
        return result;
    }

}
