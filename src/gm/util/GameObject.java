package gm.util;

import gm.scenes.Scene;
import gm.blockparty.Services;
import gm.tasks.Task;
import gm.tasks.TaskManager;
import net.jodah.typetools.TypeResolver;

import java.util.ArrayList;

public abstract class GameObject {

    public static final int HIGHEST_PRIORITY = 0;
    private static final int PRIORITY_INTERVAL = 1000000;
    public static final int VERY_HIGH_PRIORITY = PRIORITY_INTERVAL;
    public static final int DEFAULT_PRIORITY = PRIORITY_INTERVAL * 2;
    public static final int VERY_LOW_PRIORITY = PRIORITY_INTERVAL * 3;
    public static final int LOWEST_PRIORITY = PRIORITY_INTERVAL * 4;

    private ArrayList<EventManager.EntryKey> registeredHandlers;
    private TaskManager taskManager;
    private boolean isAlive = true;
    private ArrayList<GameObject> children;
    private GameObject parent;

    public GameObject() {
        Scene scene = Services.scenes.getCurrentScene();
        assert scene != null;
        scene.addObject(this);
        this.children = new ArrayList<>();
        this.taskManager = new TaskManager();
    }

    public int getPriority() {
        return DEFAULT_PRIORITY;
    }

    private ArrayList<EventManager.EntryKey> getRegisteredHandlers() {
        if (registeredHandlers == null) {
            registeredHandlers = new ArrayList<>();
        }
        return registeredHandlers;
    }

    final public void destroy() {
        _destroy();
    }

    public void _destroy() {
        if (isAlive) {
            isAlive = false;
            clearTasks();
            onDestroy();

            if (parent != null) {
                parent.removeChild(this);
            }

            for (int i = children.size() - 1; i >= 0; i--) {
                removeChild(children.get(i));
            }
        }
    }

    final void _cleanUp() {
        if (registeredHandlers != null) {
            for (EventManager.EntryKey k : registeredHandlers) {
                Services.events.unregister(k);
            }
            registeredHandlers.clear();
        }
        children.clear();
    }

    final public GameObject getParent() {
        return parent;
    }

    final public ArrayList<GameObject> getChildren() {
        return children;
    }

    final public void addChild(GameObject child) {
        assert child != this;
        assert !children.contains(child);

        if (child.parent != null) {
            child.parent.removeChild(child);
        }

        children.add(child);
        child.parent = this;
    }

    final public void removeChild(GameObject child) {
        assert children.contains(child);
        child.parent = null;
        children.remove(child);
        child.destroy();
    }

    final public <T extends Event> EventManager.EntryKey register(EventHandler<T> handler) {
        Class c = TypeResolver.resolveRawArgument(EventHandler.class, handler.getClass());
        ArrayList<EventManager.EntryKey> handlers = getRegisteredHandlers();

        //noinspection unchecked
        EventManager.EntryKey key = Services.events.register(c, handler);
        handlers.add(key);
        return key;
    }

    final public <T extends Event> void unregister(EventManager.EntryKey key) {
        if (registeredHandlers != null) {
            ArrayList<EventManager.EntryKey> handlers = getRegisteredHandlers();
            handlers.remove(key);
            Services.events.unregister(key);
        }
    }

    final protected void fireEvent(Event e) {
        Services.events.fire(e);
    }

    final protected void queueEvent(Event e) {
        if (isAlive) {
            Services.events.queueEvent(e);
        }
    }

    final public Task addTask(Task task) {
        if (isAlive) {
            taskManager.addTask(task);
        }
        return task;
    }

    final public void clearTasks() {
        taskManager.abortAllTasks();
        // taskManager.update(); // to flush all the aborted tasks
    }

    final public boolean isAlive() {
        return isAlive;
    }

    final public void _preUpdate() {
        if (isAlive) {
            preUpdate();
        }
    }

    final public void _update() {
        if (isAlive) {
            update();
            taskManager.update();
        }
    }

    final public void _postUpdate() {
        if (isAlive) {
            postUpdate();
        }
    }

    public void _init() {
        init();
    }

    protected void init() {}

    protected void preUpdate() {
    }

    protected void update() {}

    protected void postUpdate() {
    }

    public void onDestroy() {
    }

}
