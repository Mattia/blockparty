package gm.util;

import gm.blockparty.Services;
import processing.core.PFont;

import java.io.File;
import java.util.HashMap;

public class FontManager {

    private HashMap<String, PFont> fonts;

    public FontManager() {
        fonts = new HashMap<>();
        String path = Services.processing.dataPath("") + "/fonts";

        File dataDir = new File(path);

        File[] files = dataDir.listFiles();
        if (files == null) {
            Util.log("Found no files in data directory");
        } else {
            for (File file : files) {
                String fileName = file.getName();
                int i = fileName.lastIndexOf('.');
                if (i > 0) {
                    String fileExtension = fileName.substring(i + 1);
                    if (fileExtension.equals("vlw")) {
                        PFont font = Services.processing.loadFont("fonts/"+fileName);
                        fonts.put(fileName.substring(0, i), font);
                    }
                }
            }
            if (fonts.size() == 0) {
                Util.log("Found no fonts in data directory");
            }
        }
    }

    public PFont getDefaultFont() {
        // return getFont("CenturyGothic");
        return getFont("Silkscreen");
    }

    public PFont getFont(String name) {
        return fonts.get(name);
    }

}
