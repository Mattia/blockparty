package gm.util;

import gm.blockparty.Services;

import java.awt.*;

public class Util {

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MISC
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static void log(Object o) {
		System.out.println(o);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// COLOR
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static int randomRGB() {
		final int r = (int) (Math.random() * 255);
		final int g = (int) (Math.random() * 255);
		final int b = (int) (Math.random() * 255);
		final int a = 255;
		int c = a << 24;
		c |= r << 16;
		c |= g << 8;
		c |= b;
		return c;
	}

	public static int getAlpha(int color) {
		return (color & 0xFF000000) >>> 24;
	}

	public static int setAlpha(int color, int newAlpha) {
		return (color & 0x00FFFFFF) | (newAlpha << 24);
	}

	public static int setAlpha(int color, float newAlpha) {
		return (color & 0x00FFFFFF) | (((int)(newAlpha * 255))<< 24);
	}

	public static int getComplementary(int c) {
        int a = (c & 0xFF000000) >>> 24;
        int r = (c & 0x00FF0000) >>> 16;
        int g = (c & 0x0000FF00) >>> 8;
        int b = c & 0x000000FF;
        return (a << 24) | ~((r << 16) | (g << 8) | b);
    }

	public static int lerpColor(int s, int e, float t) {
		return Services.processing.lerpColor(s, e, t);
	}

	public static int[] randomDistinctColors(int num, float minSaturation, float minValue) {
		int[] colors = new int[num];

		final float valueRange = 1.0f - minValue;
		final float satRange = 1.0f - minSaturation;

		final float hueInterval = 1.0f / num;

		for (int i = 0; i < num; i++) {
			final float sat = (float) (Math.random() * satRange + minSaturation);
			final float value = (float) (Math.random() * valueRange + minValue);
			colors[i] = Color.HSBtoRGB(hueInterval * i, sat, value);
		}

		// shuffle the colors
		for (int i = 0; i < colors.length; i++) {
			int ri = (int) (Math.random() * colors.length);
			int t = colors[i];
			colors[i] = colors[ri];
			colors[ri] = t;
		}

		return colors;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// NUMBERS
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static float clamp01(float t) {
		if (t > 1) {
			t = 1;
		} else if (t < 0) {
			t = 0;
		}
		return t;
	}

    public static int lerp(int s, int e, float t) {
        return s + (int)((e - s) * t);
    }

    public static float lerp(float s, float e, float t) {
        return s + (e - s) * t;
    }

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// VECTORS
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static class Vec2i {
		public int x, y;

		public Vec2i(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public Vec2i() {
			this.x = 0;
			this.y = 0;
		}

		public Vec2i(Vec2i v) {
			this.x = v.x;
			this.y = v.y;
		}

		public static Vec2i add(Vec2i v1, Vec2i v2) {
			return new Vec2i(v1).add(v2);
		}

		public Vec2i add(Vec2i v) {
			this.x += v.x;
			this.y += v.y;
			return this;
		}

		@Override
		public boolean equals(Object obj) {
			if (!obj.getClass().equals(this.getClass())) {
				return false;
			}
			Vec2i v = (Vec2i) obj;
			return x == v.x && y == v.y;
		}

		@Override
		public String toString() {
			return "{" + x + ", " + y + "}";
		}
	}

	public static class Vec2f {
		public float x, y;

		public Vec2f(float x, float y) {
			this.x = x;
			this.y = y;
		}

		public Vec2f() {
			this.x = 0;
			this.y = 0;
		}

		public Vec2f(Vec2f v) {
			this.x = v.x;
			this.y = v.y;
		}

		public static Vec2f add(Vec2f v1, Vec2f v2) {
			return new Vec2f(v1).add(v2);
		}
		public static Vec2f sub(Vec2f v1, Vec2f v2) {
			return new Vec2f(v1).sub(v2);
		}
		public static Vec2f mul(Vec2f v, float s) {
			return new Vec2f(v).mul(s);
		}

		public Vec2f add(Vec2f v) {
			this.x += v.x;
			this.y += v.y;
			return this;
		}

		public Vec2f add(float x, float y) {
			this.x += x;
			this.y += y;
			return this;
		}

		public Vec2f sub(Vec2f v) {
			this.x -= v.x;
			this.y -= v.y;
			return this;
		}

		public Vec2f mul(float s) {
			this.x *= s;
			this.y *= s;
			return this;
		}

		@Override
		public String toString() {
			return ("{X: " + this.x + ", Y: " + this.y + "}");
		}
	}

}
