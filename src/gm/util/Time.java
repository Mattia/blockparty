package gm.util;

import processing.core.PApplet;

public class Time {

	public static final float MAX_DELTA_TIME = 1 / 10.0f;
	private PApplet p;
	private float deltaTime = 0.01f;
	private float lastFrameStart;

	public Time(PApplet p) {
		this.p = p;
		lastFrameStart = p.millis();
		p.registerMethod("post", this);
	}

	public float getDeltaTime() {
		return deltaTime;
	}

	public void post() {
		float millis = p.millis();
		deltaTime = Math.min((millis - lastFrameStart) / 1000.0f, MAX_DELTA_TIME);
		lastFrameStart = millis;
	}

	public float now() {
		return p.millis()/1000.f;
	}

	public float elapsedTime() {
		return p.millis() / 1000.0f;
	}
}
