package gm.testclients;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import gm.blockparty.Messages;
import gm.glue.Client;
import gm.glue.JavaWebSocket;
import gm.blockparty.Config;
import gm.blockparty.Move;
import gm.blockparty.Services;

import java.util.ArrayList;

public class TestClientManager {

    ArrayList<gm.glue.Client> clients = new ArrayList<>();

    private static TestClientManager instance;

    public static void start() {
        instance = new TestClientManager();
    }

    private interface ClientAction {
        void execute(Client client);
    }

    private class ActionEntry {
        public final int weight;
        public final ClientAction action;

        private ActionEntry(int weight, ClientAction action) {
            this.weight = weight;
            this.action = action;
        }
    }

    private final static int WEIGHT_RANGE = Config.Testing.DO_NOTHING_WEIGHT + Config.Testing.JOIN_REQUEST_WEIGHT + Config.Testing.MOVE_WEIGHT + Config.Testing.DISCONNECT_WEIGHT;

    private ActionEntry[] actions;

    private TestClientManager() {
        Thread thread = new Thread(this.loop);
        thread.start();
    }

    public Runnable loop = ()-> {
        try {
            System.out.println("TEST CLIENT MANAGER STARTED");
            Thread.sleep(500);
            System.out.println("TEST CLIENT MANAGER WOKE UP");

            actions = new ActionEntry[4];

            actions[0] = new ActionEntry(Config.Testing.DO_NOTHING_WEIGHT, (c)-> {});

            actions[1] = new ActionEntry(Config.Testing.JOIN_REQUEST_WEIGHT, (c)-> {
                JsonObject data = new JsonObject();
                data.add("ID", new JsonPrimitive(c.getId()));
                c.createMessage(Messages.JOIN_REQUEST).withData(data).send();
            });

            actions[2] = new ActionEntry(Config.Testing.MOVE_WEIGHT, (c)-> {
                JsonObject data = new JsonObject();
                Move[] moves = Move.values();
                Move randomMove = moves[Services.rng.nextInt(moves.length)];
                JsonPrimitive moveString = new JsonPrimitive(randomMove.toString());
                data.add("ID", new JsonPrimitive(c.getId()));
                data.add("move_type", moveString);
                c.createMessage(Math.random() > 0.5?Messages.PLAYER_MOVE_STARTED : Messages.PLAYER_MOVE_ENDED).withData(data).send();
            });

            actions[3] = new ActionEntry(Config.Testing.DISCONNECT_WEIGHT, c-> {
                clients.remove(c);
                c.close();
            });

            //noinspection InfiniteLoopStatement
            while (true) {
                while (clients.size() < Config.Testing.NUM_TEST_CLIENTS) {
                    Client client = new Client(new JavaWebSocket(Config.Network.ROUTER_URL));
                    client.on(Messages.JOIN_RESPONSE, data->{});
                    client.on(Messages.CONNECTED_TO_GAME, data->{});
                    clients.add(client);
                }

                for (int i = clients.size() - 1; i >= 0; i--) {
                    Client c = clients.get(i);
                    int r = Services.rng.nextInt(WEIGHT_RANGE);
                    for (ActionEntry e : actions) {
                        if (r <= e.weight) {
                            e.action.execute(c);
                            break;
                        }
                        r -= e.weight;
                    }
                }
                Thread.sleep((long) (Config.Testing.UPDATE_INTERVAL * 1000));
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    };


}
