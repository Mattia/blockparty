package gm.scenes;

import gm.util.Util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class SceneManager {

    private ArrayList<Scene> sceneStack;

    public SceneManager() {
        sceneStack = new ArrayList<>();
    }

    public void update() {
        Scene scene = getCurrentScene();
        if (scene != null) {
            scene._update();
        }
    }

    public void pushScene(Class<? extends Scene> sceneType) {
        try {
            Scene scene = sceneType.getConstructor().newInstance();
            activateScene(scene);
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            Util.log("ERROR: Could not create scene");
            e.printStackTrace();
        }
    }

    public void popScene() {
        Scene topScene = getCurrentScene();
        if (topScene != null) {
            topScene.stop();
            topScene.destroy();
        }
        topScene = getCurrentScene();
        if (topScene != null) {
            topScene.start();
        }
    }

    public void goToScene(Class<? extends Scene> sceneType) {
        for (int i = 0; i < sceneStack.size(); i++) {
            Scene scene = sceneStack.get(i);
            if (scene.getClass().equals(sceneType)) {
                sceneStack.remove(i);
                activateScene(scene);
                return;
            }
        }
    }

    public Scene getCurrentScene() {
        if (sceneStack.size() == 0) {
            return null;
        } else {
            return sceneStack.get(sceneStack.size() - 1);
        }
    }

    private void activateScene(Scene scene) {
        Scene previousScene = getCurrentScene();
        if (previousScene != null) {
            previousScene._stop();
        }
        sceneStack.add(scene);
        scene._init();
        scene._start();
    }

}
