package gm.scenes;

import gm.blockparty.WorldTransform;
import gm.util.GameObject;
import gm.util.ObjectManager;

public abstract class Scene {

    private ObjectManager objectManager;
    private WorldTransform rootTransform;

    public Scene() {
        objectManager = new ObjectManager();
        rootTransform = new WorldTransform();
    }

    public WorldTransform getRootTransform() {
        return rootTransform;
    }

    public void addObject(GameObject object) {
        objectManager.add(object);
    }

    final void _init() {
        init();
    }

    final void _start() {
        start();
    }

    final void _update() {
        update();
        objectManager.update();
    }

    final void _stop() {
        stop();
    }

    final public void destroy() {
        objectManager.clear();
        onDestroy();
    }

    protected void init() {
    }

    protected void start() {
    }

    protected void update() {
    }

    protected void stop() {
    }

    protected void onDestroy() {
    }

}
